---
date: {{ .Date }}
weight: 1
aliases:
- /first
tags:
- ""
categories:
- ""
author: "K8sCat"
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true

# Articli 通用配置
title: "{{ replace .Name "-" " " | title }}"
brief_content: ""
cover_images:
- "<url>"
prefix_content: "这是我参与2022首次更文挑战的第3天，活动详情查看：[2022首次更文挑战](https://juejin.cn/post/7052884569032392740)。"
suffix_content: |
  ## 原文链接
  
  [{{ replace .Name "-" " " | title }}](https://k8scat.com/{{ replace .Path ".md" "" }}/)。

juejin:
  title: "{{ replace .Name "-" " " | title }}"
  tags:
  - Rust
  category: 后端
  cover_image: "<url>"
  brief_content: ""
  prefix_content: "这是我参与2022首次更文挑战的第3天，活动详情查看：[2022首次更文挑战](https://juejin.cn/post/7052884569032392740)。"

oschina:
  title: Articli 多平台内容管理 CLI 工具
  category: Golang
  technical_field: 开发技能
  privacy: false
  original_url: ""
  deny_comment: false
  download_image: false
  top: true

csdn:
  categories:
  - Golang
  tags:
  - cli
  read_type: public
  publish_status: publish
  article_type: original
  original_url: ""
  authorized_status: false
  cover_images:
  - "<url>"
---

<!--more-->
