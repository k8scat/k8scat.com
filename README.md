# k8scat.com

[Hugo](https://gohugo.io/) + [PaperMod Theme](https://github.com/adityatelange/hugo-PaperMod)

## 新建文章

```bash
hugo new posts/go/article.md

hugo new leetcode-go/article.md

hugo new translate/article.md
```

## 同步平台

- [掘金](https://juejin.cn/)
- [知乎](https://www.zhihu.com/)
- [微信公众号](https://mp.weixin.qq.com)
  - 源自开发者
- [CSDN](https://csdn.net)
- [SegmentFault](https://segmentfault.com/)
- [OSChina](https://oschina.net)
- [InfoQ](https://xie.infoq.cn/)
- [语雀](https://yuque.com)
- [头条](https://www.toutiao.com/)
- [慕课网](https://www.imooc.com/)
- [牛客网](https://blog.nowcoder.net/)
- [LeetCode](https://leetcode-cn.com/circle/notes)
- [Notion](https://www.notion.so/)
- [博客园](https://www.cnblogs.com/)
- [51CTO](https://blog.51cto.com/)
- [Go语言中文网](https://studygolang.com/)
- [V2EX](https://www.v2ex.com/)
- [腾讯云+社区](https://cloud.tencent.com/developer/user/1050940)
- [阿里云开发者社区](https://developer.aliyun.com/profile/uqqc2nvd6gfma)
- [BBS-GO](https://mlog.club/)
- [HelloWorld](https://www.helloworld.net/)
- [CNode 社区](https://cnodejs.org/)

> 国外网站

- [Medium](https://medium.com/)
- [dev.to](https://dev.to/)

## 编辑工具

- [Markdown Nice](https://www.mdnice.com/)
- [Minimalist Markdown Editor](https://chrome.google.com/webstore/detail/minimalist-markdown-edito/pghodfjepegmciihfhdipmimghiakcjf)
