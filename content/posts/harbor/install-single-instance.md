---
title: "手把手带你安装单机版的 Harbor 容器镜像服务"
date: 2021-06-07T17:41:04+08:00
weight: 1
aliases: ["/first"]
tags: ["docker", "registry"]
categories: ["harbor"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/harbor.jpeg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本文将带你快速安装一个单机版的 Harbor 容器镜像服务，可以满足个人以及中小型企业的需求。后续会整理 [《基于 Kubernetes 搭建高可用的 Harbor 集群》](https://k8scat.com/posts/)，敬请期待！

<!--more-->

## 为什么需要自建容器镜像服务

公有云服务固然快捷好用，但站在企业的角度思考时，数据的安全性将显得更为重要，这将导致在企业中使用的很多产品和服务被要求私有化、提供私有部署，以此来达到保障企业数据安全的目标。

所以在企业中，可能不会选择像阿里云的 ACR 或者 腾讯云的 TCR 这样的公有云服务，而是选择自建容器镜像服务。

## 基础环境

官方说明的是至少需要 **2核 CPU + 4G 内存 + 40G 磁盘容量**，但在个人安装后，发现其实 Harbor 本身作为容器镜像服务占用的资源并不是很多，所以 **1核2G** 也是可以跑起来的，磁盘容量则根据镜像的大小和数量进行分配即可。

我们并不是直接在宿主机上安装 Harbor，而是使用 `Docker` + `Docker Compose` 安装，所以需要安装 Docker 和 Docker Compose，版本保持最新即可。

还需要安装 `Openssl`，用于配置 Harbor 的 HTTPS 访问，以及 Docker 的证书。

## 下载安装包

Harbor 的安装包分为在线安装包和离线安装包，在线安装包会从 DockerHub 下面需要的镜像，离线安装包则预先构建好的镜像，相比在线安装包会更大，但两种安装包的整个安装过程基本是一样的，这里是[下载地址](https://goharbor.io/docs/2.2.0/install-config/download-installer/)。

```bash
curl -LO https://github.com/goharbor/harbor/releases/download/v2.2.2/harbor-online-installer-v2.2.2.tgz
tar zxf harbor-online-installer-v2.2.2.tgz
```

解压后的文件如下：

```bash
> cd harbor && ls -la

total 40
drwxr-xr-x  2 root root  4096 Jun  7 23:25 .
drwxrwxrwt. 9 root root  4096 Jun  7 23:26 ..
-rw-r--r--  1 root root  3361 May 15 17:32 common.sh
-rw-r--r--  1 root root  7840 May 15 17:32 harbor.yml.tmpl
-rwxr-xr-x  1 root root  2500 May 15 17:32 install.sh
-rw-r--r--  1 root root 11347 May 15 17:32 LICENSE
-rwxr-xr-x  1 root root  1881 May 15 17:32 prepare
```

## 配置 HTTPS

### 自建 CA

由于是测试环境，我们选择自建 CA。首先，我们通过下面的方式生成 CA 证书的私钥：

```bash
openssl genrsa -out ca.key 4096
```

使用上面生成的私钥生成 CA 证书：

```bash
openssl req -x509 -new -nodes -sha512 -days 3650 \
    -subj "/C=CN/ST=Shenzhen/L=Shenzhen/O=example/OU=Personal/CN=k8scat.com" \
    -key ca.key \
    -out ca.crt
```

### 生成域名证书

现在我们使用上面生成的 CA 证书来生成域名证书，假设这里我们使用的域名是 `harbor.k8scat.com`。同样，我们这里先生成证书的私钥：

```bash
openssl genrsa -out harbor.k8scat.com.key 4096
```

然后使用证书的私钥生成证书签名请求文件 CSR（Certificate Signing Request）：

```bash
openssl req -sha512 -new \
    -subj "/C=CN/ST=Shenzhen/L=Shenzhen/O=example/OU=Personal/CN=harbor.k8scat.com" \
    -key harbor.k8scat.com.key \
    -out harbor.k8scat.com.csr
```

还需要生成 x509 v3 扩展文件，以此来满足 SAN（Subject Alternative Name） 和 x509 v3 扩展的要求：

```bash
cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1=harbor.k8scat.com
EOF
```

最后，我们使用 `ca.crt`、`ca.key`、`harbor.k8scat.com.csr` 和 `v3.ext` 来生成我们需要的域名证书：

```bash
openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -in harbor.k8scat.com.csr \
    -out harbor.k8scat.com.crt
```

### 配置 Harbor 和 Docker 的证书

#### Harbor

这里我们将将证书存放在 `/data/cert` 下，当然也可以是其他路径：

```bash
cp harbor.k8scat.com.crt /data/cert/
cp harbor.k8scat.com.key /data/cert/
```

#### Docker

由于 Docker 守护进程会将 `.crt` 文件解释为 CA 证书，将 `.cert` 文件解释为客户端证书。所以这里我们需要将域名证书（harbor.k8scat.com.crt）的格式转换成 `harbor.k8scat.com.cert`：

```bash
openssl x509 -inform PEM -in harbor.k8scat.com.crt -out harbor.k8scat.com.cert
```

然后将域名证书和 CA 证书拷贝到 `/etc/docker/certs.d` 目录下：

```bash
cp harbor.k8scat.com.cert /etc/docker/certs.d/harbor.k8scat.com/
cp harbor.k8scat.com.key /etc/docker/certs.d/harbor.k8scat.com/
cp ca.crt /etc/docker/certs.d/harbor.k8scat.com/
```

最后，重启 Docker 服务：

```bash
systemctl restart docker
```

## 配置 harbor.yml

在配置好 HTTPS 后，这时我们还没有运行起 Harbor 容器镜像服务，我们还需要配置 `harbor.yml` 文件，这个文件是拷贝自安装包中的 `harbor.yml.tmpl` 文件：

```bash
cp harbor.yml.tmpl harbor.yml
```

我们来看一下 `harbor.yml` 文件的内容：

```yaml
# Configuration file of Harbor

# The IP address or hostname to access admin UI and registry service.
# DO NOT use localhost or 127.0.0.1, because Harbor needs to be accessed by external clients.
hostname: reg.mydomain.com

# http related config
http:
  # port for http, default is 80. If https enabled, this port will redirect to https port
  port: 80

# https related config
https:
  # https port for harbor, default is 443
  port: 443
  # The path of cert and key files for nginx
  certificate: /your/certificate/path
  private_key: /your/private/key/path

# # Uncomment following will enable tls communication between all harbor components
# internal_tls:
#   # set enabled to true means internal tls is enabled
#   enabled: true
#   # put your cert and key files on dir
#   dir: /etc/harbor/tls/internal

# Uncomment external_url if you want to enable external proxy
# And when it enabled the hostname will no longer used
# external_url: https://reg.mydomain.com:8433

# The initial password of Harbor admin
# It only works in first time to install harbor
# Remember Change the admin password from UI after launching Harbor.
harbor_admin_password: Harbor12345

# Harbor DB configuration
database:
  # The password for the root user of Harbor DB. Change this before any production use.
  password: root123
  # The maximum number of connections in the idle connection pool. If it <=0, no idle connections are retained.
  max_idle_conns: 50
  # The maximum number of open connections to the database. If it <= 0, then there is no limit on the number of open connections.
  # Note: the default number of connections is 1024 for postgres of harbor.
  max_open_conns: 1000

# The default data volume
data_volume: /data

# Harbor Storage settings by default is using /data dir on local filesystem
# Uncomment storage_service setting If you want to using external storage
# storage_service:
#   # ca_bundle is the path to the custom root ca certificate, which will be injected into the truststore
#   # of registry's and chart repository's containers.  This is usually needed when the user hosts a internal storage with self signed certificate.
#   ca_bundle:

#   # storage backend, default is filesystem, options include filesystem, azure, gcs, s3, swift and oss
#   # for more info about this configuration please refer https://docs.docker.com/registry/configuration/
#   filesystem:
#     maxthreads: 100
#   # set disable to true when you want to disable registry redirect
#   redirect:
#     disabled: false

# Trivy configuration
#
# Trivy DB contains vulnerability information from NVD, Red Hat, and many other upstream vulnerability databases.
# It is downloaded by Trivy from the GitHub release page https://github.com/aquasecurity/trivy-db/releases and cached
# in the local file system. In addition, the database contains the update timestamp so Trivy can detect whether it
# should download a newer version from the Internet or use the cached one. Currently, the database is updated every
# 12 hours and published as a new release to GitHub.
trivy:
  # ignoreUnfixed The flag to display only fixed vulnerabilities
  ignore_unfixed: false
  # skipUpdate The flag to enable or disable Trivy DB downloads from GitHub
  #
  # You might want to enable this flag in test or CI/CD environments to avoid GitHub rate limiting issues.
  # If the flag is enabled you have to download the `trivy-offline.tar.gz` archive manually, extract `trivy.db` and
  # `metadata.json` files and mount them in the `/home/scanner/.cache/trivy/db` path.
  skip_update: false
  #
  # insecure The flag to skip verifying registry certificate
  insecure: false
  # github_token The GitHub access token to download Trivy DB
  #
  # Anonymous downloads from GitHub are subject to the limit of 60 requests per hour. Normally such rate limit is enough
  # for production operations. If, for any reason, it's not enough, you could increase the rate limit to 5000
  # requests per hour by specifying the GitHub access token. For more details on GitHub rate limiting please consult
  # https://developer.github.com/v3/#rate-limiting
  #
  # You can create a GitHub token by following the instructions in
  # https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line
  #
  # github_token: xxx

jobservice:
  # Maximum number of job workers in job service
  max_job_workers: 10

notification:
  # Maximum retry count for webhook job
  webhook_job_max_retry: 10

chart:
  # Change the value of absolute_url to enabled can enable absolute url in chart
  absolute_url: disabled

# Log configurations
log:
  # options are debug, info, warning, error, fatal
  level: info
  # configs for logs in local storage
  local:
    # Log files are rotated log_rotate_count times before being removed. If count is 0, old versions are removed rather than rotated.
    rotate_count: 50
    # Log files are rotated only if they grow bigger than log_rotate_size bytes. If size is followed by k, the size is assumed to be in kilobytes.
    # If the M is used, the size is in megabytes, and if G is used, the size is in gigabytes. So size 100, size 100k, size 100M and size 100G
    # are all valid.
    rotate_size: 200M
    # The directory on your host that store log
    location: /var/log/harbor

  # Uncomment following lines to enable external syslog endpoint.
  # external_endpoint:
  #   # protocol used to transmit log to external endpoint, options is tcp or udp
  #   protocol: tcp
  #   # The host of external endpoint
  #   host: localhost
  #   # Port of external endpoint
  #   port: 5140

#This attribute is for migrator to detect the version of the .cfg file, DO NOT MODIFY!
_version: 2.2.0

# Uncomment external_database if using external database.
# external_database:
#   harbor:
#     host: harbor_db_host
#     port: harbor_db_port
#     db_name: harbor_db_name
#     username: harbor_db_username
#     password: harbor_db_password
#     ssl_mode: disable
#     max_idle_conns: 2
#     max_open_conns: 0
#   notary_signer:
#     host: notary_signer_db_host
#     port: notary_signer_db_port
#     db_name: notary_signer_db_name
#     username: notary_signer_db_username
#     password: notary_signer_db_password
#     ssl_mode: disable
#   notary_server:
#     host: notary_server_db_host
#     port: notary_server_db_port
#     db_name: notary_server_db_name
#     username: notary_server_db_username
#     password: notary_server_db_password
#     ssl_mode: disable

# Uncomment external_redis if using external Redis server
# external_redis:
#   # support redis, redis+sentinel
#   # host for redis: <host_redis>:<port_redis>
#   # host for redis+sentinel:
#   #  <host_sentinel1>:<port_sentinel1>,<host_sentinel2>:<port_sentinel2>,<host_sentinel3>:<port_sentinel3>
#   host: redis:6379
#   password:
#   # sentinel_master_set must be set to support redis+sentinel
#   #sentinel_master_set:
#   # db_index 0 is for core, it's unchangeable
#   registry_db_index: 1
#   jobservice_db_index: 2
#   chartmuseum_db_index: 3
#   trivy_db_index: 5
#   idle_timeout_seconds: 30

# Uncomment uaa for trusting the certificate of uaa instance that is hosted via self-signed cert.
# uaa:
#   ca_file: /path/to/ca

# Global proxy
# Config http proxy for components, e.g. http://my.proxy.com:3128
# Components doesn't need to connect to each others via http proxy.
# Remove component from `components` array if want disable proxy
# for it. If you want use proxy for replication, MUST enable proxy
# for core and jobservice, and set `http_proxy` and `https_proxy`.
# Add domain to the `no_proxy` field, when you want disable proxy
# for some special registry.
proxy:
  http_proxy:
  https_proxy:
  no_proxy:
  components:
    - core
    - jobservice
    - trivy

# metric:
#   enabled: false
#   port: 9090
#   path: /metrics
```

这里我们主要修改下面这几个配置：

- hostname = `harbor.k8scat.com`
- https.certificate = `/data/cert/harbor.k8scat.com.crt`
- https.private_key = `/data/cert/harbor.k8scat.com.key`
- harbor_admin_password = `xxx`
- database.password = `xxx`

我们这次安装的 Harbor 没有使用其他组件，例如 `Notary`、`Trivy` 和 `Chart`，所以相关的配置可以忽略。

## 启动 Harbor

```bash
./install.sh
```

## 参考文档

- [Harbor Installation Prerequisites](https://goharbor.io/docs/2.2.0/install-config/installation-prereqs/)
- [Download the Harbor Installer](https://goharbor.io/docs/2.2.0/install-config/download-installer/)
- [Run the Installer Script](https://goharbor.io/docs/2.2.0/install-config/run-installer-script/)
- [Configure the Harbor YML File](https://goharbor.io/docs/2.2.0/install-config/configure-yml-file/)
- [Configure HTTPS Access to Harbor](https://goharbor.io/docs/2.2.0/install-config/configure-https/)
