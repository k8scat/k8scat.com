---
title: "高性能的内网穿透工具 frp"
date: 2021-06-09T23:24:42+08:00
weight: 1
aliases: ["/first"]
tags: ["frp"]
categories: ["frp"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/FRP-thumbnail.jpeg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本文将分享一个很好用的内网穿透工具 [frp](https://github.com/fatedier/frp)，以及一些具体的使用场景。

<!--more-->

## 什么是 frp

frp 是一个专注于内网穿透的高性能的反向代理应用，支持 TCP、UDP、HTTP、HTTPS 等多种协议。可以将内网服务以安全、便捷的方式通过具有公网 IP 节点的中转暴露到公网。

## 为什么使用 frp

通过在具有公网 IP 的节点上部署 frp 服务端，可以轻松地将内网服务穿透到公网，同时提供诸多专业的功能特性，这包括：

- 客户端服务端通信支持 TCP、KCP 以及 Websocket 等多种协议。
- 采用 TCP 连接流式复用，在单个连接间承载更多请求，节省连接建立时间。
- 代理组间的负载均衡。
- 端口复用，多个服务通过同一个服务端端口暴露。
- 多个原生支持的客户端插件（静态文件查看，HTTP、SOCK5 代理等），便于独立使用 frp 客户端完成某些工作。
- 高度扩展性的服务端插件系统，方便结合自身需求进行功能扩展。
- 服务端和客户端 UI 页面。

## 安装

可以在 Github 的 [Release](https://github.com/fatedier/frp/releases) 页面中进行下载：

```bash
curl -LO https://github.com/fatedier/frp/releases/download/v0.37.0/frp_0.37.0_linux_amd64.tar.gz
tar zxf frp_0.37.0_linux_amd64.tar.gz
```

压缩包里包含了客户端和服务端的两个可执行文件，以及对应的配置文件：

```bash
> tree -L 1 frp_0.37.0_linux_amd64
frp_0.37.0_linux_amd64
├── frpc # 客户端程序
├── frpc_full.ini # 对应客户端程序的详细配置文件
├── frpc.ini # 对应客户端程序的简单配置文件
├── frps # 服务端程序
├── frps_full.ini # 对应服务端程序的详细配置文件
├── frps.ini # 对应服务端程序的简单配置文件
├── LICENSE
└── systemd
    ├── frpc.service # 客户端的 systemd 服务配置文件
    ├── frpc@.service # 客户端的 systemd 模板文件
    ├── frps.service  # 服务端的 systemd 服务配置文件
    └── frps@.service # 服务端的 systemd 模板文件
```

## 使用

首先，这个工具需要一个公网服务器配置使用，配置文件可以参考详细的配置文件进行编写，后面会讲到自己常用的几个场景下的配置。

先启动服务端：

```bash
./frps -c ./frps.ini
```

再启动客户端：

```bash
./frpc -c ./frpc.ini
```

Tip：如果需要在后台长期运行，可以结合 `systemd` 或者 `supervisor` 进行使用。

## 使用场景

### 统一的服务端配置

```toml
[common]
bind_port = 9999

authentication_method = token
token = yourtokenhere

vhost_http_port = 10001
vhost_https_port = 10002
```

配置说明：

- `bind_port` 是客户端连接使用的端口号。
- 为了安全起见，会给服务端加上 token 认证，客户端被要求使用服务端配置的 token 才可以连接。
- `vhost_http_port` 和 `vhost_https_port` 在自定义域名访问时需要设置。

下面将都是基于这个服务端配置的一些场景下的客户端配置。

### SSH 连接内网服务器

```toml
[common]
server_addr = x.x.x.x
server_port = 9999

token = yourtokenhere

[ssh]
type = tcp
local_ip = 127.0.0.1
local_port = 22
remote_port = 6666
```

配置说明：

- `server_addr` 是服务端所在服务器的公网 IP。
- `remote_port` 设置在公网服务器上绑定的端口号。

现在，我们就可以通过 SSH 连接内网服务器：

```bash
ssh -p 6666 k8scat@x.x.x.x
```

### 暴露内网 HTTP 服务

#### TCP 类型

这种方式不需要在服务端设置 `vhost_http_port` 和 `vhost_https_port`：

```toml
[common]
server_addr = x.x.x.x
server_port = 9999

token = yourtokenhere

[web]
type = tcp
local_ip = 127.0.0.1
local_port = 3333
remote_port = 6666
```

访问：[http://x.x.x.x:6666](http://x.x.x.x:6666)

#### HTTP 类型

这种方式需要在服务端设置 `vhost_http_port` = 6666，也就是和客户端配置的 `remote_port` 相同。

```toml
[common]
server_addr = x.x.x.x
server_port = 9999

token = yourtokenhere

[web]
type = http
local_ip = 127.0.0.1
local_port = 3333
remote_port = 6666
custom_domains = x.x.x.x
```

同样访问：[http://x.x.x.x:6666](http://x.x.x.x:6666)

### 静态文件下载服务

在没用 frp 之前，使用的是 Nginx 搭建的静态文件服务器，只适用于公网服务器，如果是内网，还是得用 frp！下面是使用 frp 搭建静态文件下载服务的客户端配置：

```toml
[common]
server_addr = x.x.x.x
server_port = 9999

token = yourtokenhere

[file-server]
type = tcp
remote_port = 6666
plugin = static_file
plugin_local_path = /home/k8scat/files
plugin_strip_prefix = download
plugin_http_user = k8scat
plugin_http_passwd = yourpasswd
```

配置说明：

- `plugin_local_path` 指定本地文件存放的路径。
- `plugin_strip_prefix` 指定下载的 URL 的前缀，比如需要下载 `/home/k8scat/files/a.tgz` 文件，那么 URL 将会是 [http://x.x.x.x:6666/download/a.tgz](http://x.x.x.x:6666/download/a.tgz)。
- `plugin_http_user` 和 `plugin_http_passwd` 为了安全起见，可以设置下载时的 HTTP 身份验证。
