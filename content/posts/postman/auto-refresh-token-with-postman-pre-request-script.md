---
title: "使用 Postman 的预请求脚本实现自动刷新接口凭证"
date: 2021-06-03T01:57:06+08:00
weight: 1
aliases: ["/first"]
tags: ["postman", "pre-request script"]
categories: ["devtool"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://www.nexmo.com/wp-content/uploads/2020/10/Blog_Postman2_1200x600.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

Postman 是一个可以帮你甚至你的团队快速开发 API 的工具。

近期，在 Postman 上发现一个非常不错的功能 - **预请求脚本**（Pre-request Script），即在一个 `Request` 请求之前可以写 `JavaScript` 脚本做自动化的任务。

本文将主要介绍如何使用预请求脚本实现自动刷新接口凭证。

<!--more-->

## 关于 [Postman](https://www.postman.com/)

> Postman 是一个为接口开发的可以多人协作的平台。Postman 的各种功能可以很好地简化构建一个接口的步骤和提高团队协作的效率，从而以更快的速度去创造更好的接口！

![](https://raw.githubusercontent.com/storimg/img/4126c59e8181272b03dfcb9efe6a3be1345f56fa/k8scat.com/postman-supports.png)

## 预请求脚本

在预请求脚本里，由 `pm` 这个对象提供大部分重要的功能，包括 `pm.environment`、`pm.sendRequest` 等，
可以用于测试我们的请求内容和响应结果。

在 Postman 中，你可以创建一个 `Environment` 的集合，使用 Environment 可以统一设置一些变量，比如 `baseUrl`，然后在 Request 的地址栏像下面这样使用：

```
{{baseUrl}}/user/get
```

这时，在预请求脚本里，我们可以通过下面的方式获取和设置 Environment 的内容：

```js
// 获取 token 环境变量
const token = pm.environment.has('token')

// 设置 token 环境变量
pm.environment.set('token', 'xxx')
```

预请求脚本还有很多其他的能力，具体可以看这个 [文档](https://learning.postman.com/docs/writing-scripts/script-references/postman-sandbox-api-reference/#scripting-workflows)。

## 自动刷新接口凭证

现在有一个场景：某个接口服务要求请求是需要带上凭证 token，而且这个 token 的有效期只有两小时，如果没有预请求脚本，我们需要每隔两小时重新获取一次 token，这个动作是手动，所以显得十分繁琐。

但是有了预请求脚本，我们可以在接口集合 Collections 下设置预请求脚本，让接口集合下的所有请求 Request 执行前都会先执行预请求脚本，这个预请求脚本会自动获取接口凭证 token，并设置到环境变量中，此时，接口请求就是正确的了！

下面是我写的预请求脚本：

```js
const d = new Date();
const t = d.getTime();
const lastRefreshTokenTime = parseInt(pm.environment.get("lastRefreshTokenTime"));
if (t-lastRefreshTokenTime < 3600000) {
    return
}

const url = pm.environment.replaceIn("{{baseUrl}}/api/refreshToken")
const req = {
    url: url,
    method: 'POST',
    header: {
        'Content-Type': 'application/json'
    },
    body: {
        mode: 'raw',
        raw: JSON.stringify({
            appId: pm.environment.get('AppID'),
            appSecret: pm.environment.get('AppSecret')
        })
    }
};
pm.sendRequest(req, function (err, response) {
    if (response.code === 200) {
        const data = response.json()
        pm.environment.set('token', data.token)
        pm.environment.set('lastRefreshTokenTime', t)
        return
    }
    console.log(`Error: ${response}`)
});
```

上面的预请求脚本除了会自动刷新 token 外，还会通过环境变量 `lastRefreshTokenTime` 判断上一次获取 token 的时间，如果时间间隔超过一个小时，才会刷新 token 并设置 `lastRefreshTokenTime` 为当前时间戳，否则说明 token 仍有效，不会去刷新 token。
