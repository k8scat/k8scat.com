---
title: "使用 systemd 管理 frp 服务"
date: 2021-06-11T21:53:41+08:00
weight: 1
aliases: ["/first"]
tags: ["systemd", "service"]
categories: ["linux"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/systemd.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

frp 是一个高性能的内网穿透工具，本文将讲解如何使用 `systemd` 进行管理 frp 服务，保持 frp 持续正常地工作。

<!--more-->

## 关于 systemd

`systemd` 是一个专用于 `Linux` 操作系统的系统与服务管理器。当作为启动进程(`PID=1`)运行时，它将作为初始化系统运行，也就是启动并维护各种用户空间的服务。

在 CentOS 7 上查看一下 systemd 进程：

```bash
> ps aux | grep systemd | grep -v grep
root         1  0.0  0.0 191412  3536 ?        Ss   Mar19  21:14 /usr/lib/systemd/systemd --switched-root --system --deserialize 21
root       452  0.0  0.1  98664 46160 ?        Ss   Mar19  19:42 /usr/lib/systemd/systemd-journald
root       475  0.0  0.0  43944  1000 ?        Ss   Mar19   0:00 /usr/lib/systemd/systemd-udevd
dbus       629  0.0  0.0  54080  1584 ?        Ss   Mar19  20:35 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation
root       643  0.0  0.0  24692  1508 ?        Ss   Mar19  10:01 /usr/lib/systemd/systemd-logind
```

> 注意，在 MaxOS 上，没有 systemd，替而代之的是 [launchctl](https://web.archive.org/web/20180218090359/https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man1/launchctl.1.html)。

## systemd 服务单元

systemd 拥有 11 个单元类型，分别是：服务(service)、套接字(socket)、设备(device)、挂载点(mount)、自动挂载点(automount)、 启动目标(target)、交换分区或交换文件(swap)、被监视的路径(path)、任务计划(timer)、 资源控制组(slice)、一组外部创建的进程(scope)。

其中，服务单元是以 `.service` 为后缀的单元文件，封装了一个被 `systemd` 监视与控制的进程。

服务的单元文件包括三个部分：`[Service]`、`[Unit]` 和 `[Install]`。

在 GitHub 上下载的 frp 压缩包中已经包含了对应 frpc 和 frps 的服务的 **单元文件**和**单元模板文件**。下面我们将分别讲解基于单元文件和基于单元模板文件进行管理 frps 服务（frpc 服务类似）。

### 基于单元文件管理 frps 服务

我们先使用 frps 的单元文件（`frps.service`）进行管理 frps 服务。

我们先看一下 `frps.service` 的文件内容：

```toml
[Unit]
Description=Frp Server Service # 服务描述
After=network.target # frps 将会在 network.service 启动完毕之后再启动

[Service]
Type=simple # 不论进程是否启动成功，systemctl start 都执行成功
User=nobody # 设置进程在执行时使用的用户
Restart=on-failure # on-failure 表示仅在服务进程异常退出时重启
RestartSec=5s # 设置在重启服务前暂停多长时间
ExecStart=/usr/bin/frps -c /etc/frp/frps.ini # 在启动该服务时需要执行的命令行

[Install]
WantedBy=multi-user.target # 用于 systemctl enable 时创建软连接
```

将这个文件拷贝到 `/usr/lib/systemd/system/` 目录下：

```bash
sudo cp /path/to/frps.service /usr/lib/systemd/system/
```

启动 frps：

```bash
sudo systemctl start frps
```

查看 frps 服务的状态：

```bash
> sudo systemctl start frps
● frps.service - Frp Server Service
   Loaded: loaded (/usr/lib/systemd/system/frps.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-06-12 01:37:46 CST; 1s ago
 Main PID: 23430 (frps)
    Tasks: 5
   Memory: 8.0M
   CGroup: /system.slice/frps.service
           └─23430 /usr/bin/frps -c /etc/frp/frps.ini

Jun 12 01:37:46 iZwz93g2xezuhc0vzfgsy6Z systemd[1]: Started Frp Server Service.
Jun 12 01:37:46 iZwz93g2xezuhc0vzfgsy6Z frps[23430]: 2021/06/12 01:37:46 [I] [root.go:200] frps uses config file: /etc/frp/frps.ini
Jun 12 01:37:46 iZwz93g2xezuhc0vzfgsy6Z frps[23430]: 2021/06/12 01:37:46 [I] [service.go:192] frps tcp listen on 0.0.0.0:7000
Jun 12 01:37:46 iZwz93g2xezuhc0vzfgsy6Z frps[23430]: 2021/06/12 01:37:46 [I] [root.go:209] frps started successfully
```

停止 frps：

```bash
sudo systemctl stop frps
```

开启 frps 服务开机自启动：

```bash
> sudo systemctl enable frps
Created symlink from /etc/systemd/system/multi-user.target.wants/frps.service to /usr/lib/systemd/system/frps.service.
```

禁用 frps 服务开机自启动：

```bash
> sudo systemctl disable frps
Removed symlink /etc/systemd/system/multi-user.target.wants/frps.service.
```

### 基于单元模板文件管理 frps 服务

frps 的单元模板文件是 `frps@.service`，相比单元文件，多了一个 `@` 符号。我们可以在 `@` 符号和 `.service` 文件后缀的中间加入一个参数（这个参数会被用于设置一些替换符，比如 `%i`），比如 `frps@test.service`，这样我们就基于单元模板文件实例化了一个 `frps@test` 服务。

我们先看下 `frps@.service` 的内容：

```toml
[Unit]
Description=Frp Server Service
After=network.target

[Service]
Type=simple
User=nobody
Restart=on-failure
RestartSec=5s
ExecStart=/usr/bin/frps -c /etc/frp/%i.ini

[Install]
WantedBy=multi-user.target
```

我们可以发现，`ExecStart` 中 frps 使用的配置文件的名称使用了替换符 `%i`，这个 `%i` 的值是 `@` 符号和 `.service` 文件后缀中间的内容，比如 `frps@test.service`，那么 `%i` 就等于 `test`，也是说 frps 的配置文件就变成了 `/etc/frp/test.ini`，这样就可以根据不同的实例选择不同的配置文件。

同样，我们需要先将 `frps@.service` 文件拷贝到 `/usr/lib/systemd/system/` 目录下：

```bash
sudo cp /path/to/frps@.service /usr/lib/systemd/system/
```

如果我们需要基于 `frps@.service` 单元模板文件实例化（启动）一个服务时，比如 `frps@test.service`，我们只需要执行 `systemctl start` 命令，这样就会基于单元模板文件实例化一个服务（我们并不需要拥有一个单元文件 `/usr/lib/systemd/system/frps@test.service`）：

```bash
sudo systemctl start frps@test
```

查看 frps@test 服务的状态：

```bash
> sudo systemctl status frps@test
● frps@test.service - Frp Server Service
   Loaded: loaded (/usr/lib/systemd/system/frps@.service; disabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-06-12 02:02:51 CST; 5s ago
 Main PID: 31171 (frps)
   CGroup: /system.slice/system-frps.slice/frps@test.service
           └─31171 /usr/bin/frps -c /etc/frp/test.ini

Jun 12 02:02:51 iZwz93g2xezuhc0vzfgsy6Z systemd[1]: Started Frp Server Service.
Jun 12 02:02:51 iZwz93g2xezuhc0vzfgsy6Z frps[31171]: 2021/06/12 02:02:51 [I] [root.go:200] frps uses config file: /etc/frp/test.ini
Jun 12 02:02:51 iZwz93g2xezuhc0vzfgsy6Z frps[31171]: 2021/06/12 02:02:51 [I] [service.go:192] frps tcp listen on 0.0.0.0:7000
Jun 12 02:02:51 iZwz93g2xezuhc0vzfgsy6Z frps[31171]: 2021/06/12 02:02:51 [I] [root.go:209] frps started successfully
```

停止 frps@test 服务：

```bash
sudo systemctl stop frps@test
```

开启 frps@test 服务开机自启动（**指向的是单元模板文件**）：

```bash
> sudo systemctl enable frps@test 
Created symlink from /etc/systemd/system/multi-user.target.wants/frps@test.service to /usr/lib/systemd/system/frps@.service.
```

禁用 frps@test 服务开机自启动：

```bash
> sudo systemctl disable frps@test
Removed symlink /etc/systemd/system/multi-user.target.wants/frps@test.service.
```
