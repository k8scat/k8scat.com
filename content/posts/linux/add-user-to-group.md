---
title: "Linux 添加用户至用户组"
date: 2020-12-24T11:42:43+08:00
weight: 1
aliases: ["/first"]
tags: ["usermod", "adduser", "group"]
categories: ["linux"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/add-user-to-group.jpeg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本教程通过使用 Linux 命令行的几个的示例，逐步向你展示如何在 Linux 上将用户添加到用户组中，以及如何在 Linux 上添加用户和组。这些命令应该可以在任何 Linux 发行版上工作，并且已经在CentOS、Debian 和 Ubuntu 上进行过测试。

<!--more-->

## 添加一个新用户到用户组

一个 Linux 用户可以有一个主组（Primary group）和一个或多个附属组（Secondary groups）。这些组可以在创建用户时作为 `adduser` 命令的参数。

所有命令都必须以 `root` 用户的身份执行。在 Ubuntu 上，请在所有命令前加上 `sudo`，或者运行`sudo -s` 切换到 `root` 用户。

### 添加用户组

作为第一步，我将添加两个新的用户组，分别是 `family` 和 `friends`：

```bash
groupadd family
groupadd friends
```

### 添加新用户至单个用户组

下面我将一个新用户 `tom`，同时将用户添加到用户组 `family`。`family` 用户组将通过使用 `-G` 参数作为一个附属组添加。

```bash
useradd -G family tom
```

### 添加新用户到多个用户组

`tom` 现在是 `family` 用户组的一个用户。参数 `-G` 允许指定多个用户组，每个用户组之间使用逗号进行分隔。如果要将用户 `tom` 添加到 `family` 和 `friends` 两个用户组，使用下面的命令：

```bash
useradd -G family,friends tom
```

### 设置用户密码

请注意，新的 Linux 用户 `tom` 还没有密码，所以无法登录。要设置此用户的密码，可以执行下面的命令：

```bash
passwd tom
```

并在命令请求时输入两次新密码。

在上面的示例中，我们将用户 `tom` 添加到辅助组中，`adduser` 命令自动创建了一个新的主组，并将该组分配为主组。

- **用户名**: `tom`
- **主组**: `tom`
- **附属组**: `family`（或者使用第二个案例添加 `family` 和 `friends` 两个附属组)

### 设置新的主组

也许你想在添加 `tom` 用户时，设置主组为 `family`（而不是默认创建的 `tom` 用户组），附属组为 `friends`，可以使用这个命令：

```bash
useradd -g family -G friends tom
```

使用 `man` 命令可以获得 `useradd` 命令的所有命令行选项的详细描述：

```bash
man useradd
```

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/useradd-manpage.png)

## 将已有的用户添加至用户组

对于这个任务，我们将使用 `usermod` 命令。`usermod` 命令可以修改用户的各种选项，包括用户的组成员关系。

首先，我将添加第三个用户组 `colleagues`：

```bash
groupadd colleagues
```

### 使用 `usermod`

我将 `colleagues` 用户组作为附属组添加到用户 `tom`：

```bash
usermod -a -G colleagues tom
```

命令解释：`-a` 表示 `append`，它只能与 `-G` 选项（附属组）组合使用。所以最终我们将 `tom` 用户添加到 `colleagues` 用户组中，这个用户组是用户的一个附属组。

`-G` 选项可以指定多个用户组，每个用户组之间使用逗号进行分隔。例如：`-G group1,group2,group3`。

如果想要修改 `tom` 用户的主组为 `family`，可以使用命令：

```bash
usermod -g family tom
```

使用 `man` 命令可以获取 `usermod` 命令的所有命令行选项的详细说明：

```bash
man usermod
```

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/usermod-manpage.png)

> 翻译自 [Linux: Add User to Group](https://www.faqforge.com/linux/linux-add-user-to-group/)
