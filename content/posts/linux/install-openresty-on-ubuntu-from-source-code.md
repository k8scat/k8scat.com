---
title: "在 Ubuntu 上使用源码安装 OpenResty"
date: 2021-07-10T11:39:25+08:00
weight: 1
aliases: ["/first"]
tags: ["OpenResty", "Ubuntu"]
categories: ["linux"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/resty-image.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本文将介绍如何在 Ubuntu 上使用源码安装 OpenResty。

<!--more-->

## 目标

- Ubuntu 18.04
- OpenResty 1.19.3.2

## 安装依赖

- 启用 [HTTP 基本状态模块](http://nginx.org/en/docs/http/ngx_http_stub_status_module.html)：`--with-http_stub_status_module`
- 启用 [HTTP gzip 静态文件压缩模块](http://nginx.org/en/docs/http/ngx_http_gzip_static_module.html)：--with-http_gzip_static_module
- 启用 [HTTP/2 模块](http://nginx.org/en/docs/http/ngx_http_v2_module.html)：--with-http_v2_module

> `zlib1g-dev`: the HTTP gzip module requires the zlib library.

```bash
apt-get update -y
apt-get install -y libpcre3-dev \
  libssl-dev \
  perl \
  make \
  build-essential \
  curl \
  zlib1g-dev
```

## 下载 OpenResty

```bash
cd /opt
curl -LO https://openresty.org/download/openresty-1.19.3.2.tar.gz
tar zxf openresty-1.19.3.2.tar.gz
```

## 安装 OpenResty

```bash
.configure \
  --with-http_gzip_static_module \
  --with-http_v2_module \
  --with-http_stub_status_module

make
make install
```

## 使用 systemd 管理 OpenResty 服务

### 编写 Service 文件

在 `/usr/lib/systemd/system` 目录下创建一个 `openresty.service` 文件，文件内容如下：

```toml
# Stop dance for OpenResty
# =========================
#
# ExecStop sends SIGSTOP (graceful stop) to OpenResty's nginx process.
# If, after 5s (--retry QUIT/5) nginx is still running, systemd takes control
# and sends SIGTERM (fast shutdown) to the main process.
# After another 5s (TimeoutStopSec=5), and if nginx is alive, systemd sends
# SIGKILL to all the remaining processes in the process group (KillMode=mixed).
#
# nginx signals reference doc:
# http://nginx.org/en/docs/control.html
#
[Unit]
Description=The OpenResty Application Platform
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/usr/local/openresty/nginx/logs/nginx.pid
ExecStartPre=/usr/local/openresty/nginx/sbin/nginx -t -q -g 'daemon on; master_process on;'
ExecStart=/usr/local/openresty/nginx/sbin/nginx -g 'daemon on; master_process on;'
ExecReload=/usr/local/openresty/nginx/sbin/nginx -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /usr/local/openresty/nginx/logs/nginx.pid
TimeoutStopSec=5
KillMode=mixed

[Install]
WantedBy=multi-user.target
```

### 启动 OpenResty

```bash
# 设置自启动
systemctl enable openresty

# 启动 OpenResty
systemctl start openresty

# 查看 OpenResty 服务状态
systemctl status openresty
● openresty.service - The OpenResty Application Platform
   Loaded: loaded (/usr/lib/systemd/system/openresty.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2021-07-10 11:36:07 CST; 26min ago
  Process: 12735 ExecStart=/usr/local/openresty/nginx/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
  Process: 12734 ExecStartPre=/usr/local/openresty/nginx/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
 Main PID: 12736 (nginx)
    Tasks: 2 (limit: 1126)
   CGroup: /system.slice/openresty.service
           ├─12736 nginx: master process /usr/local/openresty/nginx/sbin/nginx -g daemon on; master_process on;
           └─12737 nginx: worker process

Jul 10 11:36:07 iZj6c0qglm7rjjctj7zxnfZ systemd[1]: Starting The OpenResty Application Platform...
Jul 10 11:36:07 iZj6c0qglm7rjjctj7zxnfZ systemd[1]: openresty.service: Failed to parse PID from file /usr/local/openresty/nginx/logs/nginx.pid:
Jul 10 11:36:07 iZj6c0qglm7rjjctj7zxnfZ systemd[1]: Started The OpenResty Application Platform.
```

## 一键安装脚本

[GitHub Gist](https://gist.github.com/k8scat/0adca267c15ae9f3ed39770803e82ec3)
