---
title: "使用 OpenSSL 生成自签名证书"
date: 2021-02-09T16:54:14+08:00
weight: 1
aliases: ["/first"]
tags: ["SSL", "TLS", "CA", "OpenSSL"]
categories: ["DevOps"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://img-blog.csdnimg.cn/20210209193537645.jpg"
    alt: "TLS/SSL Certificates"
    caption: "TLS/SSL Certificates"
    relative: false
comments: true
---

当使用不是受信任的第三方 CA 而是自签名的 CA 时，一些浏览器会提示不安全，比如谷歌的 Chrome 浏览器。

<!--more-->

## 生成 CA 证书

### 生成私钥

```bash
openssl genrsa -out ca.key 4096
```

### 生成证书

根据组织或个人设置 `-subj` 选项。如果使用域名（FQDN）访问，需要将 `CN` 设置为域名。

```bash
openssl req -x509 -new -nodes -sha512 -days 3650 \
-subj "/C=CN/ST=Beijing/L=Beijing/O=example/OU=Personal/CN=yourdomain.com" \
-key ca.key \
-out ca.crt
```

## 生成服务器端证书

证书通常包含两部分：`.crt` 文件和 `.key` 文件，例如接下来生成的 `yourdomain.com.crt` 和 `yourdomain.com.key`。

### 生成私钥

```bash
openssl genrsa -out yourdomain.com.key 4096
```

### 生成证书签名请求文件（CSR）

根据组织或个人设置 `-subj` 选项。如果使用域名（FQDN）访问，需要将 `CN` 设置为域名，并且使用域名作为私钥文件和证书签名请求文件的文件名。

```bash
openssl req -sha512 -new \
-subj "/C=CN/ST=Beijing/L=Beijing/O=example/OU=Personal/CN=yourdomain.com" \
-key yourdomain.com.key \
-out yourdomain.com.csr
```

> 关于 `-subj` 选项：
>  
> `-subj` 用于设置 Subject Name  
> 其中 C 表示 Country or Region  
> ST 表示 State/Province  
> L 表示 Locality  
> O 表示 Organization  
> OU 表示 Organization Unit  
> CN 表示 Common Name

### 生成 x509 v3 扩展文件

无论是使用域名还是 IP 进行访问，都必须使用 x509 v3 扩展文件才能生成符合 SAN（Subject Alternative Name）和 x509 v3扩展要求的域名证书。通过设置 DNS 入口映射到域名。

```bash
cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage=digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage=serverAuth
subjectAltName=@alt_names

[alt_names]
DNS.1=yourdomain.com
DNS.2=yourdomain
DNS.3=hostname
EOF
```

### 使用 `v3.ext` 生成域名证书

将 `yourdomain.com` 替换成需要生成证书的域名。

```bash
openssl x509 -req -sha512 -days 3650 \
-extfile v3.ext \
-CA ca.crt -CAkey ca.key -CAcreateserial \
-in yourdomain.com.csr \
-out yourdomain.com.crt
```
