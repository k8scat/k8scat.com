---
title: "基于 VirtualBox + Ubuntu 16 搭建 Kubernetes 集群"
date: 2021-06-06T21:14:25+08:00
weight: 1
aliases: ["/first"]
tags: ["Cluster", "VirtualBox", "Ubuntu"]
categories: ["Kubernetes"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/kubernetes.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

这篇文章记录了如何在一台机器上使用 VirtualBox + Ubuntu 16 搭建 Kubernetes 的整个过程，包括其中遇到的一些问题以及解决办法。

<!--more-->

## 关于 Kubernetes

下面是一段来自维基百科的关于 Kubernetes 的解释：

> Kubernetes（常简称为 K8s）是用于自动部署、扩展和管理「容器化（containerized）应用程序」的开源系统。该系统由 Google 设计并捐赠给 Cloud Native Computing Foundation（今属 Linux 基金会）来使用。
>
> 它旨在提供“跨主机集群的自动部署、扩展以及运行应用程序容器的平台”。它支持一系列容器工具，包括Docker等。

Kubernetes 可以为我们提供 `服务发现和负载均衡`、`存储编排`、`自动部署和回滚`、`自动完成装箱计算`、`自我修复` 和 `密钥与配置管理` 的能力。

## 基础环境准备

### 安装 VirtualBox

VirtualBox 是一种功能强大的虚拟机软件，而且是开源免费的，这是[下载地址](https://www.virtualbox.org/wiki/Downloads)，安装 VirtualBox 非常简单，这里我就不赘述了。

### 下载 Ubuntu 16 系统镜像

这里我选择了 Ubuntu 16 作为系统镜像，当然你也可以使用其他系统，比如 CentOS 等，Ubuntu 16 的[下载地址](https://releases.ubuntu.com/16.04/)。

### 虚拟机 x3

安装好了 VirtualBox，下载了 Ubuntu 16 的镜像后，我们首先需要搭建三台 Ubuntu 16 的虚拟机。这个新建虚拟机的过程也是比较简单的，一步一步往下走就可以了。新建完成后，我们需要对每台虚拟机进行相应的配置，配置时使用的用户应该是 `root` 用户。

#### 虚拟机 IP

由于我们使用的是虚拟机，我们会给每台虚拟机配置网卡，让每台虚拟机都可以上网的，这里有两种方式：

1. 使用 `桥接网卡`，每台虚拟机的 IP 将会是宿主机网段的，支持虚拟机上网
2. 使用 `NAT 网络` + 端口转发，网段自行设置，支持虚拟机上网

大家可以使用其中任意一种方式给虚拟机配置网卡，从而让虚拟机可以上网。

需要注意的是，**在集群搭建完成后，集群中的每个节点的 IP 要求保持不变，否则节点需要重新加入。**

简单的方式就是让虚拟机不关机，而是进入睡眠状态，下次只需唤醒即可。

在集群中，我们使用的是内网地址，可以通过 `ifconfig` 或者 `ip addr` 找到每台虚拟机对应的内网地址：

```bash
> ifconfig

enp0s3    Link encap:Ethernet  HWaddr 08:00:27:6f:23:2a  
          inet addr:10.0.2.4  Bcast:10.0.2.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe6f:232a/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:3277016 errors:0 dropped:0 overruns:0 frame:0
          TX packets:3385793 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:1084480916 (1.0 GB)  TX bytes:2079122979 (2.0 GB)
```

这台虚拟机（master）的地址就是 `10.0.2.4`。

#### 配置主机名

Kubernetes 的节点名称是由主机名决定的，所以我们可以分别设置三台虚拟机的主机名为 `master`、`node1` 和 `node2`，通过修改 `/etc/hosts` 文件来修改主机名，需要重启虚拟机：

```bash
# /etc/hosts
10.0.2.4 master
10.0.2.5 node1
10.0.2.6 node2
```

#### SSH 无密连接

在虚拟机运行起来后，我们要做的第一件事就是要连通这三台虚拟机，即配置 SSH 无密连接。

首先在其中的一台虚拟机上生成 SSH 的公私钥：

```bash
ssh-keygen -t rsa -C 'k8scat@gmail.com' -f ~/.ssh/id_rsa -q -N ''
```

> 关于 `ssh-keygen` 的参数说明：  
> `-t rsa` 指定加密算法为 `RSA`  
> `-C 'k8scat@gmail.com'` 用于提供一个备注，表明私钥的生成者  
> `-f ~/.ssh/id_rsa` 指定私钥生成的位置  
> `-q -N ''` 表示不对私钥加密码，以及使用静默的方式  

将公私钥分发给另外两台虚拟机，并在三台虚拟机上都将公钥（`~/.ssh/id_rsa.pub`）的内容写进 `~/.ssh/authorized_keys` 文件中，同时设置 `~/.ssh/authorized_keys` 文件的权限为 `400`：

```bash
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
chmod 400 ~/.ssh/authorized_keys
```

配置完成后，我们将可以通过以下方式在其中一个虚拟机上连接另一台虚拟机了：

```bash
# 在 master 节点上
ssh root@node1
```

## Kubernetes 集群搭建

在弄好三台虚拟机后，我们便可以开始搭建一个拥有三个节点的 Kubernetes 的集群了。

### 安装 Docker

```bash
apt-get update -y
apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# INSTALL DOCKER ENGINE
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io

# Configure Docker to start on boot
systemctl enable docker.service
systemctl enable containerd.service

# Start Docker
systemctl start docker
```

### 安装 kubeadm、kubelet 和 kubectl

这里使用的是阿里云的镜像源：

```bash
# 更新 apt 包索引并安装使用 Kubernetes apt 仓库所需要的包
apt-get update -y
apt-get install -y apt-transport-https ca-certificates curl

# 下载 Google Cloud 公开签名秘钥
# curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add -

# 添加 Kubernetes apt 仓库
# echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
echo "deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# 更新 apt 包索引，安装 kubelet、kubeadm 和 kubectl，并锁定其版本
apt-get update -y
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
```

### 关闭 SWAP

编辑 `/etc/fstab` 文件并注释掉 `swap` 分区的配置：

```/etc/fstab
#/dev/mapper/master--vg-swap_1 none            swap    sw              0       0
```

### 预先下载镜像

获取 `kubeadm init` 需要使用到的镜像列表：

```bash
> kubeadm config images list

k8s.gcr.io/kube-apiserver:v1.21.1
k8s.gcr.io/kube-controller-manager:v1.21.1
k8s.gcr.io/kube-scheduler:v1.21.1
k8s.gcr.io/kube-proxy:v1.21.1
k8s.gcr.io/pause:3.4.1
k8s.gcr.io/etcd:3.4.13-0
k8s.gcr.io/coredns/coredns:v1.8.0
```

k8s 的镜像源对于国内用户是可望而不可即的，但我们可以先拉到国内的镜像仓或者可以使用的镜像仓，比如阿里云的容器镜像服务 ACR 和 Docker 的官方镜像仓 DockerHub。

我们可以新建一个 GitHub 代码仓，里面只有一个 Dockerfile，其内容如下：

```Dockerfile
FROM k8s.gcr.io/kube-apiserver:v1.21.0
```

然后在阿里云的容器镜像服务 ACR 中新建一个镜像，并关联这个 GitHub 代码仓，构建出来的镜像就是我们要的 k8s 镜像，比如上面的 `k8s.gcr.io/kube-apiserver:v1.21.1`，但在使用的时候需要重新给镜像打标签。

在 ACR 中构建好了所有需要的镜像后，使用下面这个脚本可以快速处理给镜像打标签的任务：

```bash
# Pull images from aliyun registry
kubeadm config images list | sed -e 's/^/docker pull /g' -e 's#k8s.gcr.io#registry.cn-shenzhen.aliyuncs.com/k8scat#g' -e 's#/coredns/coredns#/coredns#g' | sh -x

# Tag images
docker images | grep k8scat | awk '{print "docker tag",$1":"$2,$1":"$2}' | sed -e 's#registry.cn-shenzhen.aliyuncs.com/k8scat#k8s.gcr.io#2' | sh -x
docker tag k8s.gcr.io/coredns:v1.8.0 k8s.gcr.io/coredns/coredns:v1.8.0

# Remove images
docker images | grep k8scat | awk '{print "docker rmi",$1":"$2}' | sh -x
```

### 初始化 master 节点

`10.0.2.4` 是 master 节点的 IP 地址，设置 pod 网段为 `192.168.16.0/20`：

```bash
> kubeadm init --apiserver-advertise-address=10.0.2.4 --pod-network-cidr=192.168.16.0/20

kubeadm join 10.0.2.4:6443 --token ioshf8.40n8i0rjsehpigcl \
    --discovery-token-ca-cert-hash sha256:085d36848b2ee8ae9032d27a444795bc0e459f54ba043500d19d2c6fb044b065
```

### 加入 node 节点

```bash
kubeadm join 10.0.2.4:6443 --token ioshf8.40n8i0rjsehpigcl \
    --discovery-token-ca-cert-hash sha256:085d36848b2ee8ae9032d27a444795bc0e459f54ba043500d19d2c6fb044b065
```

### 分发 kubectl 配置文件

```bash
# node1
scp master:/etc/kubernetes/admin.conf /etc/kubernetes/admin.conf
echo 'export KUBECONFIG="/etc/kubernetes/admin.conf"' >> /etc/profile
source /etc/profile
```

### 安装网络插件

这里我们使用的是 `Weave Net`：

```bash
# curl -L "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" > weave-net.yaml

# With IPALLOC_RANGE
kubectl apply -f https://gist.githubusercontent.com/k8scat/c6a1aa5a1bdcb8c220368dd2db69bedf/raw/da1410eea6771c56e93f191df82206be8e722112/k8s-weave-net.yaml
```
