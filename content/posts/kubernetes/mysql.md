---
title: "基于 Kubernetes 部署 MySQL 数据库"
date: 2021-06-12T23:24:51+08:00
weight: 1
aliases: ["/first"]
tags: ["blog"]
categories: ["blog"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/k8s-mysql.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本文将介绍如何基于 Kubernetes 部署 MySQL 数据库。

<!--more-->

## 创建服务 Service

创建一个 `Service` 为即将部署的 MySQL 数据库固定连接的 IP，同时提供负载均衡，下面是 `mysql-service.yaml` 文件的内容：

```yaml
apiVersion: v1
kind: Service
metadata:
  name: mysql
spec:
  selector:
    app: mysql
  ports:
    - port: 3306
```

上述配置创建一个名称为 `mysql` 的 `Service` 对象，它会将请求代理到使用 TCP 端口 3306，并且具有标签 `app=mysql` 的 Pod 上。

创建资源：

```bash
kubectl create -f mysql-service.yaml
```

## 创建持久卷 PV

创建一个 MySQL 的持久卷 `mysql-pv.yaml`（当 Pod 不再存在时，Kubernetes 也会销毁临时卷；不过 Kubernetes 不会销毁 持久卷。）：

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce # 卷可以被一个节点以读写方式挂载
  hostPath:
    path: "/mnt/data"
```

创建资源：

```bash
kubectl create -f mysql-pv.yaml
```

## 创建持久卷声明 PVC

持久卷是集群中的资源，而持久卷声明是对这些资源的请求，也被用来执行对资源的声明检查。下面我们将创建名称为 `mysql-pvc` 的持久卷声明 `mysql-pvc.yaml`：

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi
```

创建资源：

```bash
kubectl create -f mysql-pvc.yaml
```

## 部署 MySQL

在 3306 端口上使用 MySQL 5.7 的镜像创建 Pod，`mysql-deployment.yaml`：

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
spec:
  selector:
    matchLabels:
      app: mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
        - image: mysql:5.7
          name: mysql
          env:
            - name: MYSQL_ROOT_PASSWORD # 生产环境中请使用 secret
              value: password
          ports:
            - containerPort: 3306
              name: mysql
          volumeMounts:
            - name: mysql-data
              mountPath: /var/lib/mysql
      volumes:
        - name: mysql-data
          persistentVolumeClaim:
            claimName: mysql-pvc
```

创建资源：

```bash
kubectl create -f mysql-deployment.yaml
```

## 连接 MySQL

```bash
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -hmysql -ppassword
```
