---
title: "How to disable `auto cd` in zsh with oh-my-zsh"
date: 2021-10-07T21:18:10+08:00
weight: 1
aliases: ["/first"]
tags: ["zsh", "oh-my-zsh"]
categories: ["zsh"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

I have a binary file named x in my path (not the current folder, but it is in the PATH), and also a folder with the same name in the current working directory. If I type x, I want the binary to execute, but instead it cd's into that folder. How do I fix this?

<!--more-->

Just put a `unsetopt AUTO_CD` in your `.zshrc`.

Refer: [How to disable "auto cd" in zsh with oh-my-zsh](https://qastack.cn/unix/126719/how-to-disable-auto-cd-in-zsh-with-oh-my-zsh)
