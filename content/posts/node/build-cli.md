---
title: "如何基于 Node.js 构建并发布一个 CLI 命令行工具"
date: 2022-01-19T22:43:59+08:00
weight: 1
aliases: ["/first"]
tags: ["blog"]
categories: ["node"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://github.com/storimg/img/blob/master/k8scat.com/build-node-cli.jpeg?raw=true"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

CLI 命令行工具是很常见的应用，之前会用 Golang/Rust 等语言构建过一些 CLI 工具，但是这次将基于 Node.js 构建一个 CLI 命令行工具，同时将其发布到官方的 NPM Registry [npmjs.com](https://npmjs.com/) 上，方便其他人可以下载使用。

<!--more-->

### 功能介绍

使用 CLI 命令行工具发送飞书群机器人消息。

```shell
lark-notify https://open.feishu.cn/open-apis/bot/v2/hook/{key} Hello World
```

### 构建二进制项目

#### 初始化项目

先新建一个项目目录，同时使用 `npm init` 进行初始化项目

```shell
mkdir node-lark-notify
cd node-lark-notify
npm init -y
```

进入项目目录下，创建一个 `bin` 目录，并在 `bin` 目录下创建一个 `index.js` 文件，这是 CLI 命令行工具的入口文件，先来打印一个 `Hello World`：

```js
#!/usr/bin/env node
console.log('Hello World');
```

#### CLI 项目基础配置

```json
{
  "bin": {
    "lark-notify": "bin/index.js"
  }
}
```

#### 本地安装

通过上面的配置，我们就可以在本地安装这个项目，然后执行二进制命令：

```shell
sudo npm install -g .
```

安装成功后，我们可以执行 `lark-notify` 命令行工具，这个命令行工具会输出一个 `Hello World`：

```shell
$ lark-notify
Hello World
```

#### 完善项目功能

由于需要请求飞书机器人的 webhook_url，所以我们选择安装 `node-fetch` 依赖用于请求接口

```shell
npm install node-fetch
```

完善功能代码如下：

```js
#!/usr/bin/env node
import fetch from "node-fetch";

const lark_notify = async (webhookUrl, msg) => {
  const data = {
    msg_type: "text",
    content: {
      text: msg
    }
  }
  const response = await fetch(webhookUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return response.json();
}

const main = () => {
  if (process.argv.length <= 3) {
    console.log("Usage: lark-notify <webhookUrl> <message>");
    return;
  }

  const webhookUrl = process.argv[2];
  const msg = process.argv.slice(3).join(" ");
  lark_notify(webhookUrl, msg).then(res => {
    console.log(res);
  })
}

main()
```

重新在本地安装：

```shell
sudo npm install -g .
```

现在来使用一下：

```shell
lark-notify https://open.feishu.cn/open-apis/bot/v2/hook/01e26b08-2095-4b55-86c6-cd897e8d0e93 Hello World
```

![node-lark-notify.jpg](https://github.com/storimg/img/blob/master/k8scat.com/node-lark-notify.jpg?raw=true)

![lark-group-msg.png](https://github.com/storimg/img/blob/master/k8scat.com/lark-group-msg.png?raw=true)

### 发布到官方 NPM Registry

为了可以让更多人用到我们的项目，我们可以将项目发布到官方 NPM Registry，这样其他人就可以下载使用了。

首先，登录 npm：

```shell
$ npm login
npm notice Log in on https://registry.npmjs.org/
Username: k8scat
Password: 
Email: (this IS public) k8scat@gmail.com
Enter one-time password from your authenticator app: 010212
Logged in as k8scat on https://registry.npmjs.org/.
```

![npm-login.png](https://github.com/storimg/img/blob/master/k8scat.com/npm-login.png?raw=true)

使用 `npm publish` 发布项目，这里需要指定参数 `--access public` 以表示这个项目是公开的：

```shell
$ npm publish --access public                               
npm notice 
npm notice 📦  @k8scat/node-lark-notify@1.0.2
npm notice === Tarball Contents === 
npm notice 57B   .editorconfig
npm notice 1.1kB LICENSE      
npm notice 329B  README.md    
npm notice 678B  bin/index.js 
npm notice 810B  package.json 
npm notice === Tarball Details === 
npm notice name:          @k8scat/node-lark-notify                
npm notice version:       1.0.2                                   
npm notice filename:      @k8scat/node-lark-notify-1.0.2.tgz      
npm notice package size:  1.8 kB                                  
npm notice unpacked size: 3.0 kB                                  
npm notice shasum:        aba3806ac3c5c3ba1387a938e861486e7b72e9f2
npm notice integrity:     sha512-9NLDgX2t8dWhv[...]4sp2PIkzZThJQ==
npm notice total files:   5                                       
npm notice 
+ @k8scat/node-lark-notify@1.0.2
```

![npm-publish.png](https://github.com/storimg/img/blob/master/k8scat.com/npm-publish.png?raw=true)

现在就可以在官方 NPM Registry 上搜索 `@k8scat/node-lark-notify` 这个项目了，同时可以在本地安装这个 CLI 工具：

```shell
sudo npm install -g @k8scat/node-lark-notify
```

使用 `lark-notify` 命令行工具发送消息：

```shell
lark-notify https://open.feishu.cn/open-apis/bot/v2/hook/{key} Hello World
```

### 项目开源

[node-lark-notify](https://github.com/k8scat/node-lark-notify) 项目已经开源了，欢迎大家在 GitHub 上点赞或者提交 PR。
