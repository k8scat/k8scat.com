---
title: "在 Linux 上使用二进制文件安装 Node.js"
date: 2021-05-20T06:08:34-04:00
weight: 1
aliases: ["/first"]
tags: ["node.js", "linux", "binary"]
categories: ["node.js"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com8-linux-nodejs-1.jpg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

在 Linux 上安装软件会比在 Windows 安装软件更加透明，因为我们可以知道安装了哪些东西。

<!--more-->

## 下载并解压

下载地址：[https://nodejs.org/dist/](https://nodejs.org/dist/)

```bash
version="v12.22.1"
curl -LO https://nodejs.org/dist/v12.22.1/node-${version}-linux-x64.tar.gz

sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-${version}-linux-x64.tar.xz -C /usr/local/lib/nodejs
```

## 配置环境变量

编辑 `~/.zshrc` 或者 `~/.profile`，添加以下内容

```bashrc
export PATH=/usr/local/lib/nodejs/node-v14.17.0-linux-x64/bin:$PATH
```

使用 `source ~/.zshrc` 或者 `source ~/.profile` 立即生效。

## 验证安装结果

```bash
~ npm -v
6.14.13

~ npm version
{
  npm: '6.14.13',
  ares: '1.17.1',
  brotli: '1.0.9',
  cldr: '38.1',
  icu: '68.2',
  llhttp: '2.1.3',
  modules: '83',
  napi: '8',
  nghttp2: '1.42.0',
  node: '14.17.0',
  openssl: '1.1.1k',
  tz: '2020d',
  unicode: '13.0',
  uv: '1.41.0',
  v8: '8.4.371.23-node.63',
  zlib: '1.2.11'
}

~ npx -v  
6.14.13
```

## 参考文档

- [nodejs/help Installation](https://github.com/nodejs/help/wiki/Installation#how-to-install-nodejs-via-binary-archive-on-linux)
