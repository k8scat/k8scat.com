---
title: "还原一个全栈开发者的开发环境"
date: 2021-06-05T15:06:39+08:00
weight: 1
aliases: ["/first"]
tags: ["vscode", "remote-ssh", "git", "docker", "zsh", "ohmyzsh", "proxy"]
categories: ["devtool"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/social-remote-ssh.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

已经不知道换了多少次开发环境，从 Windows 到 Linux，再到 MacOS，更多还是 Linux，但每次都是凭着记忆还原曾经熟悉的那个开发环境，这个过程要花好几天，所以这次我决定记录下这整个过程。

<!--more-->

## 开发环境

最近，由于开发用的一台服务器将要到期了，所以新的服务器需要重新配置环境，借此机会记录下我还原整个开发环境的过程。

我是一个全栈后端开发者，主要写 Golang 等，使用的 IDE 是 `VSCode`，再加上它的利器 `Remote - SSH`，远程开发让我很少再使用本地的环境进行开发了，因为这可以让我在切换设备后，仍可以正常工作！

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/architecture.png)

我的开发环境（远程服务器）包括以下内容：

- **Proxy**
- Oh-My-Zsh
- Go
- Docker
- Git

### Proxy

**所有的开始，都要从配置代理开始，这将会加快整个过程。**

作为一名国内开发者，很多时候都需要访问或者下载一些国外的资源，当然可以通过一些国内源代替，但是最好的解决办法还是配置一个代理。这里说明一下如何在 Linux 上配置代理，我们只需要在 `~/.bash_profile` 文件里添加下面的内容：

```bash
proxy_ip="127.0.0.1"
proxy_port="7890"
proxy_addr="$proxy_ip:$proxy_port"
http_proxy="http://$proxy_addr"
socks5_proxy="socks5://$proxy_addr"
alias proxy='export https_proxy=$http_proxy \
http_proxy=$http_proxy \
ftp_proxy=$http_proxy \
rsync_proxy=$http_proxy \
all_proxy=$socks5_proxy \
no_proxy="127.0.0.1,localhost"'
alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'
```

保存后，执行下面的命令即可生效：

```bash
source ~/.bash_profile
```

在需要使用代理的时候，执行 `proxy` 即可开启代理，不需要的时候，执行 `unproxy` 即可关闭。

在后面我们配置好 `Oh-My-Zsh` 后，可以将上面写在 `~/.bash_profile` 里的内容移到 `~/.zshrc` 文件中。

### Oh-My-Zsh

`bash` 固然可以，但我更喜欢使用 `zsh` + `Oh-My-Zsh`，因为它更加好看，也拥有更多的插件可以使用，可以帮助我节省很多时间。

`Oh-My-Zsh` 是 `zsh` 的一个框架，所以安装 `Oh-My-Zsh` 之前，需要先安装 `zsh`，我用的系统是 `CentOS 7`，所以需要使用下面的方法进行安装：

```bash
sudo yum update && sudo yum -y install zsh
```

如果是其他系统，可以参考这个 [文档](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH) 进行安装 `zsh`。

安装完 `zsh` 后，我们就可以安装 `Oh-My-Zsh` 了，只需一步：

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

到这里，就安装好了 `Oh-My-Zsh`，但我还会用到一些插件，比如 `git`、`zsh-syntax-highlighting`、`zsh-autosuggestions` 等，下面是我的配置文件 `~/.zshrc`：

```bash
export ZSH="/home/k8scat/.oh-my-zsh"
ZSH_THEME="robbyrussell"

plugins_folder="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins"
syntax_highlight_plugin="${plugins_folder}/zsh-syntax-highlighting"
[[ ! -d "$syntax_highlight_plugin" ]] && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $syntax_highlight_plugin
autosuggestions_plugin="${plugins_folder}/zsh-autosuggestions"
[[ ! -d "$autosuggestions_plugin" ]] && git clone https://github.com/zsh-users/zsh-autosuggestions $autosuggestions_plugin
[[ -z $(pip list | grep -E "^wakatime ") ]] && pip install --user wakatime
wakatime_plugin="${plugins_folder}/wakatime"
[[ ! -d "$wakatime_plugin" ]] && git clone https://github.com/sobolevn/wakatime-zsh-plugin.git $wakatime_plugin
[[ ! -s "$HOME/.wakatime.cfg" ]] && cat > $HOME/.wakatime.cfg <<EOF
[settings]
api_key = xxx
EOF
plugins=(git zsh-syntax-highlighting zsh-autosuggestions wakatime docker docker-compose)

source $ZSH/oh-my-zsh.sh

export GOPRIVATE="github.com/private"
export GO111MODULE="auto"
export GOPROXY="https://goproxy.io,direct"
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

proxy_ip="127.0.0.1"
proxy_port="7890"
proxy_addr="$proxy_ip:$proxy_port"
http_proxy="http://$proxy_addr"
socks5_proxy="socks5://$proxy_addr"
alias proxy='export https_proxy=$http_proxy \
http_proxy=$http_proxy \
ftp_proxy=$http_proxy \
rsync_proxy=$http_proxy \
all_proxy=$socks5_proxy \
no_proxy="127.0.0.1,localhost,192.168.8.152,192.168.8.154,192.168.8.155"'
alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'
proxy

export PATH=$PATH:$HOME/.local/bin
```

### Go

作为一名 Go 开发者，必不可少的当然是安装 Go。由于我一开始就配置了代理，所以使用的是国外的下载源，安装步骤主要如下：

```bash
curl -LO https://golang.org/dl/go1.16.5.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz
```

然后需要配置一些环境变量：

```bash
export GOPRIVATE="github.com/private"
export GO111MODULE="auto"
export GOPROXY="https://goproxy.io,direct"
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
```

### Docker

Docker 真的是现代软件开发中必不可少的一个技术了，不使用 Docker 都 Out 了！

也确实，Docker 在日常开发中也有很大的帮助，比如需要一个 MySQL 数据库，我们只需一行命令就可以跑起来：

```bash
docker run -d -p 3306:3306 mysql:8
```

这比以前安装一个 MySQL 折腾半天可要简单太多了！所以如果你现在还不会使用 Docker 的话，赶紧学起来吧！

下面是如何在 CentOS 上安装 Docker：

```bash
sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io

# 让 Docker 服务开机自启动
sudo systemctl enable docker
# 启动 Docker
sudo systemctl start docker

# 验证
sudo docker run hello-world
```

其他系统安装 Docker 可以参考这个 [文档](https://docs.docker.com/engine/install/)。

### Git

版本控制是开发过程中必不可少的一个环节，而 Git 就是这个环节的一大利器。在 CentOS 上，我们可以快速使用下面的命令安装 Git：

```bash
sudo yum install -y git
```

但是，这样安装的 Git 的版本比较低，或者不是我们想要的版本，我们可以选择去 GitHub 上面下载需要的发布版本，然后基于源码编译安装，下面是如何安装 Git v2.29.2 的过程：

```bash
# 安装依赖
sudo yum install -y gcc openssl-devel expat-devel curl-devel

# 下载源码
curl -LO https://github.com/git/git/archive/v2.29.2.tar.gz
tar zxf v2.29.2.tar.gz
cd git-2.29.2
make prefix=/usr
sudo make prefix=/usr install
```

## 本地 VSCode

到此，远程服务器的环境大致算是完成了，然后本地 VSCode 需要安装插件 `Remote - SSH`，然后配置 `~/.ssh/config` 文件，添加以下内容：

```config
Host dev-server
    HostName 40.18.95.22
    User k8scat
    Port 9232
    IdentityFile ~/.ssh/id_rsa
```

最后，便可以开始远程开发之旅了，下班后回家再也不用带电脑了，回到家继续连上远程 Coding...
