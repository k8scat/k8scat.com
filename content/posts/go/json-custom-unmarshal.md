---
title: "Go json 自定义 Unmarshal 避免判断 nil"
date: 2021-12-04T14:54:17+08:00
weight: 1
aliases: ["/first"]
tags: ["json"]
categories: ["go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

[腾讯《Go安全指南》](https://github.com/Tencent/secguide/blob/main/Go%E5%AE%89%E5%85%A8%E6%8C%87%E5%8D%97.md)中提到[【必须】nil指针判断](https://github.com/Tencent/secguide/blob/main/Go%E5%AE%89%E5%85%A8%E6%8C%87%E5%8D%97.md#112%E5%BF%85%E9%A1%BBnil%E6%8C%87%E9%92%88%E5%88%A4%E6%96%AD)：进行指针操作时，必须判断该指针是否为nil，防止程序panic，尤其在进行结构体Unmarshal时。但如果每次使用都要判断一下是否 nil 防止 panic的话，那么这样的代码就会比较麻烦，这里我们可以使用一个自定义的方法，来避免这种情况。

<!--more-->

## 使用默认的 Unmarshal 方法

```go
package main

import (
    "encoding/json"
    "fmt"
)

type A struct {
    Name string `json:"name"`
    Num  *int   `json:"num"`
}

func main() {
    var a A
    err := json.Unmarshal([]byte(`{"name": "hsowan"}`), &a)
    if err != nil {
        panic(err)
    }
    fmt.Println(a.Name)
    // 每次使用都要判断一下是否 nil 防止 panic
    if a.Num != nil {
        fmt.Println(*a.Num)
    }
}
```

## 自定义的 Unmarshal 方法

当字段为 nil 时，在 Unmarshal 时进行初始化，这样就可以避免使用的时候发生 panic，
同时也不需要在使用的时候判断是否为 nil 了。

```go
package main

import (
    "encoding/json"
    "fmt"
)

type A struct {
    Name string `json:"name"`
    Num  *int   `json:"num"`
}

func (a *A) UnmarshalJSON(b []byte) error {
    type alias A
    aux := (*alias)(a)
    if err := json.Unmarshal(b, &aux); err != nil {
        return err
    }
    if aux.Num == nil {
        aux.Num = new(int)
    }
    return nil
}

func main() {
    var a A
    err := json.Unmarshal([]byte(`{"name": "hsowan"}`), &a)
    if err != nil {
        panic(err)
    }
    fmt.Println(a.Name)
    fmt.Println(*a.Num)
}
```

## 参考

- [[译]自定义Go Json的序列化方法](https://colobu.com/2020/03/19/Custom-JSON-Marshalling-in-Go/)
