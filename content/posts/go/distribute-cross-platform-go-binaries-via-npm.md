---
title: "Distribute Cross Platform Go Binaries via Npm"
date: 2022-01-24T23:09:32+08:00
weight: 1
tags:
- ""
categories:
- ""
author: "K8sCat"
showToc: true
TocOpen: true
draft: true
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true

juejin:
  title: "Distribute Cross Platform Go Binaries via Npm"
  tags:
  - Rust
  category: 后端
  cover_image: ""
  brief_content: ""
  prefix_content: "这是我参与2022首次更文挑战的第3天，活动详情查看：[2022首次更文挑战](https://juejin.cn/post/7052884569032392740)。"
  suffix_content: |
    ## Powered by

    本文由 [Articli](https://github.com/k8scat/Articli.git) 工具自动发布，原文链接：[Distribute Cross Platform Go Binaries via Npm](https://k8scat.com/posts/go/distribute-cross-platform-go-binaries-via-npm/)。
---

https://github.com/sanathkr/go-npm

https://blog.xendit.engineer/how-we-repurposed-npm-to-publish-and-distribute-our-go-binaries-for-internal-cli-23981b80911b

<!--more-->
