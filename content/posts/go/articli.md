---
title: Articli 多平台内容管理 CLI 工具
date: "2022-01-23T17:07:45+08:00"
weight: 1
aliases:
- /first
tags:
- CLI
categories:
- Go
author: K8sCat
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
  image: https://github.com/storimg/img/blob/master/k8scat.com/go-write.png?raw=true
  alt: <alt text>
  caption: <text>
  relative: false
comments: true
juejin:
  tags:
  - Go
  category: 后端
  cover_image: https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/8e346a7f8558498d9837efd033ff5763~tplv-k3u1fbpfcp-watermark.image?
  brief_content: Articli 是一个可以管理多个平台内容的命令行工具，通过解析 Markdown 文件内容以及调用平台接口，实现文章的发布、更新等功能。
  prefix_content: 这是我参与2022首次更文挑战的第3天，活动详情查看：[2022首次更文挑战](https://juejin.cn/post/7052884569032392740)。
  suffix_content: |
    ## 原文链接

    [Articli 多平台内容管理 CLI 工具](https://k8scat.com/posts/go/articli/)。
  article_create_time: "2022-01-23 17:15:27"
  article_id: "7056326060228476941"
  article_update_time: "2022-02-06 14:43:08"
  draft_id: "7056327710867456007"
oschina:
  title: Articli 多平台内容管理 CLI 工具
  category: Golang
  technical_field: 开发技能
  privacy: false
  original_url: ""
  deny_comment: false
  download_image: false
  top: true
  article_id: "5415564"
  article_update_time: "2022-02-06 14:44:10"
  article_create_time: "2022-01-26 23:03:53"
csdn:
  title: Articli 多平台内容管理 CLI 工具
  brief_content: Articli 是一个可以管理多个平台内容的命令行工具，通过解析 Markdown 文件内容以及调用平台接口，实现文章的发布、更新等功能。
  categories:
  - Golang
  - 后端
  tags:
  - cli
  - csdn
  read_type: public
  publish_status: publish
  article_type: original
  original_url: ""
  authorized_status: false
  cover_images:
  - https://github.com/storimg/img/blob/master/k8scat.com/go-write.png?raw=true
  suffix_content: |
    ## Powered by

    本文由 [Articli](https://github.com/k8scat/Articli.git) 工具自动发布。
  article_create_time: "2022-02-06 17:42:18"
  article_id: "122682038"
  article_update_time: "2022-02-06 17:53:18"
---

**Articli** 是一个可以管理多个平台内容的命令行工具，
通过解析 `Markdown` 文件内容以及调用平台接口，实现内容管理。

最终目标是基于 **本地文件** + **Git 代码仓** 管理所有的文章，
并且可以通过命令行操作以及 CI/CD，实现文章在各个平台的发布、更新等功能。
这样做的好处有：

- 数据安全，既发布到了第三方平台，又可以通过 **Git 代码仓**管理，避免因平台问题导致数据丢失
- 可以实现自动化，比如文章自动在多个平台发布、更新
- 面向程序员的 CLI 工具，可以实现更多个性化的操作

为本项目点赞将鼓励作者继续完善下去，欢迎提出建议、Bug、PR。

[GitHub 地址](https://github.com/k8scat/Articli)

<!--more-->

## 支持的平台

- [GitHub](https://github.com)
- [掘金](https://juejin.cn)
- [开源中国](https://oschina.net)

## 安装

### NPM

```shell
npm install -g @k8scat/articli
```

### Homebrew

```shell
# 使用 tap
brew tap k8scat/tap
# 安装 Articli
brew install acli

# 直接安装
brew install k8scat/tap/acli

# 升级
brew update
brew upgrade k8scat/tap/acli
```

### Docker

```shell
# 将配置文件的目录挂载到容器内
docker run \
  -it \
  --rm \
  -v $HOME/.config/articli:/root/.config/articli \
  k8scat/articli:latest \
  juejin auth login

# 升级
docker pull k8scat/articli:latest
```

### 二进制

Please download from the [releases page](https://github.com/k8scat/Articli/releases).

### 源码编译

```shell
git clone https://github.com/k8scat/articli.git
cd articli
make
```

## 文章模板

我们将使用文件内容开头 `---` 之间的数据作为文章的配置信息（元数据），
根据配置信息在不同平台上创建或更新文章，参考 [文章模板](./templates/article.md)。

## 使用说明

所有的命令都可以通过 `-h` 或 `--help` 参数查看帮助信息。

### 查看版本

```shell
acli version
```

### GitHub

#### 登录

使用 GitHub Token 进行登录

```shell
# 交互式登录
acli github auth login

# 从标准输入获取 Token
acli github auth login --with-token < token.txt
```

#### 上传文件

```shell
# 上传 README.md 文件到 testrepo 仓库
acli github file upload --repo testrepo README.md

# 使用网络资源
# 使用 -p 指定在仓库中存储的路径
acli github file upload --repo testrepo \
  -p testdir/homebrew-social-card.png \
  https://brew.sh/assets/img/homebrew-social-card.png
```

#### 列取文件

```shell
# 获取代码仓 testrepo 根目录的文件列表，包括文件和目录
acli github file get --repo testrepo

# 如果 testpath 是目录，则获取代码仓 testrepo 中 testpath 目录下的文件；
# 如果 testpath 是文件，则只获取该文件
acli github file get --repo testrepo --path testpath
```

![articli-github-file-upload.png](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/articli-github-file-get.png)

#### 删除文件

```shell
# 使用 -o 或 --owner 可以指定仓库的 owner
acli github file delete --owner testowner --repo testrepo --path testdir/filename.txt
```

### 掘金 CLI

#### 登录

使用浏览器 Cookie 进行登录

```shell
# 交互式登录
acli juejin auth login

# 从标准输入获取 Cookie
acli juejin auth login --with-cookie < cookie.txt
```

#### 创建/更新文章

```shell
# create 命令可以通过识别文章的配置信息，自动选择创建或者更新文章，同时发布到掘金
acli juejin article create markdown-file.md
```

#### 查看文章列表

通过 `-k` 或 `--keyword` 关键字参数过滤文章列表

```shell
acli juejin article list -k Docker
```

#### 打开文章

使用默认浏览器打开文章

```shell
acli juejin article view 7055689358657093646
```

#### 查看分类

```shell
acli juejin category list
```

#### 查看标签

```shell
# 过滤关键字
acli juejin tag list -k Go
```

#### 缓存标签

由于标签的数量比较多，可以通过设置缓存加快读取速度

```shell
# 设置缓存
acli juejin tag cache

# 使用缓存
acli jujin tag list --use-cache
```

#### 上传图片

支持上传本地图片和网络图片

```shell
# 本地图片
acli juejin image upload leetcode-go.png

# 网络图片
acli juejin image upload https://launchtoast.com/wp-content/uploads/2021/11/learn-rust-programming-language.png
```

### 简化命令

使用 `alias` 别名进行简化命令

```shell
# 将 acli juejin 简化成 jcli
cat >> ~/.bashrc << EOF
alias jcli="acli juejin"
alias gcli="acli github"
EOF

# 生效
source ~/.bashrc

# 使用简化后的命令查看掘金的登录状态
jcli auth status
```
