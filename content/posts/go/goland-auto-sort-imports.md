---
date: 2022-02-12T21:20:26+08:00
weight: 1
aliases:
- /first
tags:
- "goland"
- "gofmt"
categories:
- "go"
author: "K8sCat"
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/go-goland.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true

# Articli 通用配置
title: "GoLand 自动对 import 进行分组排序"
brief_content: ""
cover_images:
- "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/go-goland.png"
prefix_content: ""
suffix_content: |
  ## 原文链接
  
  [Goland Auto Sort Imports](https://k8scat.com/posts/go/goland-auto-sort-imports/)。

juejin:
  tags:
  - Rust
  category: 后端
  cover_image: ""
  brief_content: ""

oschina:
  category: Golang
  technical_field: 开发技能
  privacy: false
  original_url: ""
  deny_comment: false
  download_image: false
  top: true

csdn:
  categories:
  - Golang
  tags:
  - cli
  read_type: public
  publish_status: publish
  article_type: original
  original_url: ""
  authorized_status: false
  cover_images:
  - "<url>"
---

<!--more-->

![goland-editor-codestyle-go-imports.png](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/goland-editor-codestyle-go-imports.png)

![goland-actions-on-save.png](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/goland-actions-on-save.png)
