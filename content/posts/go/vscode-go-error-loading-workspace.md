---
title: "Resolved: VSCode Go Error loading workspace"
date: 2021-09-03T23:16:37+08:00
weight: 1
aliases: ["/first"]
tags: ["VSCode", "gopls"]
categories: ["go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://go.dev/blog/gopls/features.gif"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

<!--more-->

使用 govendor 管理依赖的 Go 项目在使用 VSCode go 插件的时候出现下面的错误：

```txt
Error loading workspace: err: exit status 1: stderr: unexpected directory layout:
import path: _/home/hsowan/go/src/github.com/bangwork/bang-api/app/models 
root: /home/hsowan/go/src
dir: /home/hsowan/go/src/github.com/bangwork/bang-api/app/models
expand root: /home/hsowan/go
expand dir: /home/hsowan/go/src/github.com/bangwork/bang-api/app/models
separator: / : packages.Load error
```

临时解决办法是禁用 `gopls`，编辑项目根目录下的 `.vscode/settings.json` 文件并添加以下内容：

```json
{
    "go.useLanguageServer": false
}
```

参考：

- [Update VSCOD Getting Error loading workspace folders](https://stackoverflow.com/questions/65944416/update-vscod-getting-error-loading-workspace-folders)
- [Gopls on by default in the VS Code Go extension](https://go.dev/blog/gopls-vscode-go)
- [disable gopls in VS Code](https://github.com/golang/vscode-go/blob/master/docs/settings.md#gouselanguageserver)
