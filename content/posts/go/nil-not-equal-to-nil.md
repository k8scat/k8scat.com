---
title: "Go nil 判断引起的问题"
date: 2021-06-16T23:16:32+08:00
weight: 1
aliases: ["/first"]
tags: ["nil", ""]
categories: ["go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/go.jpeg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

代码封装是百干不厌的事，但有时候封装会导致一些问题。本文记录了个人在封装 `http` 请求时遇到的一个和 `nil` 判断有关的问题。

<!--more-->

## nil 是什么

`nil` 内置的一个变量，用来代表空值，且只有指针、`channel`、方法、接口、`map` 和切片可以被赋值为 `nil`。

`buildin/buildin.go`：

```go
// nil is a predeclared identifier representing the zero value for a
// pointer, channel, func, interface, map, or slice type.
var nil Type // Type must be a pointer, channel, func, interface, map, or slice type

// Type is here for the purposes of documentation only. It is a stand-in
// for any Go type, but represents the same type for any given function
// invocation.
type Type int
```

## 问题代码

下面的代码是我对 `http.Post` 方法的封装

```go
func (r *Request) Post(endpoint string, params *url.Values, body io.Reader, headers map[string]string, cookies map[string]string) (resp *http.Response, err error) {
    url := fmt.Sprintf("%s%s", r.BaseURL, endpoint)
    var req *http.Request
    req, err = http.NewRequest(http.MethodPost, url, body)
    if err != nil {
        return
    }
    r.setRequest(req, params, headers, cookies)
    resp, err = r.Client.Do(req)
    return
}
```

然后像下面这样使用的时候：

```go
var body *bytes.Reader
body = nil

resp, err = req.Post(endpoint, nil, body, nil, nil)
```

这时会出现空指针的错误，最终经过漫长的排查发现是在 `http.NewRequest` 里出现的空指针错误：

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/go-http-nil-err.png)

## 错误分析

指针和接口的底层实现有两部分：data 和 type。当指针和接口被显式地赋值为 `nil` 时，data 和 type 同时为 `nil`，但是将一个 type 不为 nil 但 data 为 nil 的值赋值给指针或接口时，再与 nil 作比较的结果则是 false。

## 修改代码

使用 `reflect.ValueOf(body).IsNil()` 判断 `body` 是否为空：

```go
func (r *Request) Post(endpoint string, params *url.Values, body io.Reader, headers map[string]string, cookies map[string]string) (resp *http.Response, err error) {
    url := fmt.Sprintf("%s%s", r.BaseURL, endpoint)
    var req *http.Request
    if reflect.ValueOf(body).IsNil() {
        req, err = http.NewRequest(http.MethodPost, url, nil)
    } else {
        req, err = http.NewRequest(http.MethodPost, url, body)
    }
    if err != nil {
        return
    }
    r.setRequest(req, params, headers, cookies)
    resp, err = r.Client.Do(req)
    return
}
```
