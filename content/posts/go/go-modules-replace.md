---
title: "Go modules replace 解决 Go 模块引用问题"
date: 2021-08-07T13:54:32+08:00
weight: 1
aliases: ["/first"]
tags: ["Go modules", "replace", "package", "require"]
categories: ["go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/go.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

取名是一个很有讲究的事情，但每个人都不一样。

<!--more-->

## 为什么会用到 replace

一开始，我写了一个 A 项目，代码仓名称为 `project-alpha`，但 `go.mod` 里的 `package` 设置的是 `github.com/k8scat/alpha`，
当在另一项目 B 中想要引用 A 项目的代码时，一般来说，用的是 `github.com/k8scat/project-alpha`，
因为 `go get` 会使用 `git` 去，但由于 `package` 和代码仓的名称不一样，需要使用 `replace` 进行转换一下：

```go.mod
package github.com/k8scat/beta

require github.com/k8scat/alpha v0.0.0

replace github.com/k8scat/alpha github.com/k8scat/project-alpha v1.0.0
```

使用 `go mod edit` 进行设置 replace：

```bash
go mod edit -replace github.com/k8scat/alpha=github.com/k8scat/project-alpha@v1.0.0
```

> 注意：该命令只会设置 replace，require 需要另外设置，且 require 的依赖版本被忽略，使用 replace 的依赖版本。

## replace 的其他使用场景

### 解决 golang.org 依赖无法下载的问题

```bash
go mod edit -replace golang.org/x/crypto=github.com/golang/crypto@v0.0.0-20160511215533-1f3b11f56072
```

> 注意：这里目标依赖的版本号必须符合 SemVer 规范，不能是 master 和 latest（go get 可以用）

## 参考资料

- [go get 源码](https://github.com/golang/go/blob/master/src/cmd/go/internal/modget/get.go)
- [go modules 中使用 replace 用法](https://cloud.tencent.com/developer/article/1683694)
- [Semantic Versioning](https://semver.org/)
