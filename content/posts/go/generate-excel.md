---
title: "Golang 生成 Excel 文档"
date: 2021-06-08T23:13:35+08:00
weight: 1
aliases: ["/first"]
tags: ["excel", "excelize"]
categories: ["go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/excelize-golang-library-for-reading-and-writing-xlsx-files-2.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

基于数据生成 Excel 文档是一个很常见的需求，本文将介绍如何使用 Go 的 [Excelize](https://github.com/360EntSecGroup-Skylar/excelize) 库去生成 Excel 文档，以及一些具体场景下的代码实现。

<!--more-->

## 关于 Excelize 库

`Excelize` 是 Go 语言编写的用于操作 Office Excel 文档基础库，基于 ECMA-376，ISO/IEC 29500 国际标准。可以使用它来读取、写入由 Microsoft Excel™ 2007 及以上版本创建的电子表格文档。支持 XLSX / XLSM / XLTM / XLTX 等多种文档格式，高度兼容带有样式、图片(表)、透视表、切片器等复杂组件的文档，并提供流式读写 API，用于处理包含大规模数据的工作簿。可应用于各类报表平台、云计算、边缘计算等系统。使用本类库要求使用的 Go 语言为 1.15 或更高版本。

### 性能对比

下图是一些主要的开源 Excel 库在生成 12800*50 纯文本矩阵时的性能对比（OS: macOS Mojave version 10.14.4, CPU: 3.4 GHz Intel Core i5, RAM: 16 GB 2400 MHz DDR4, HDD: 1 TB），包括 Go、Python、Java、PHP 和 NodeJS。

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/excelize-golang-library-for-reading-and-writing-xlsx-files-3.png)

## 安装

最新的版本是 `v2.4.0`：

```bash
go get github.com/360EntSecGroup-Skylar/excelize/v2
```

## 创建 Excel 文档

下面的案例中，我们创建了一个 Excel 文档，并使用 `NewSheet` 方法新建了一个 `Sheet2` 工作表，`Sheet1` 是默认创建的工作表，然后我们使用 `SetCellValue` 方法分别在 `Sheet2` 工作表的 `A2` 单元格 和 `Sheet1` 表格的 `B2` 单元格设置值，并通过使用 `SetActiveSheet` 方法设置 `Sheet2` 工作表为默认的工作表，最终调用 `SaveAs` 方法将数据写入 Excel 文档中：

```go
package main

import (
    "fmt"

    "github.com/360EntSecGroup-Skylar/excelize/v2"
)

func main() {
    f := excelize.NewFile()
    // 创建一个工作表
    index := f.NewSheet("Sheet2")
    // 设置单元格的值
    f.SetCellValue("Sheet2", "A2", "Hello world.")
    f.SetCellValue("Sheet1", "B2", 100)
    // 设置工作簿的默认工作表
    f.SetActiveSheet(index)
    // 根据指定路径保存文件
    if err := f.SaveAs("Book1.xlsx"); err != nil {
        fmt.Println(err)
    }
}
```

## 实际场景复现

### 创建工作表

工作表名称是大小写敏感的：

```go
index := f.NewSheet("Sheet2")
```

### 删除默认创建的工作表

默认创建的 Excel 文档是包含一个名为 `Sheet1` 的工作表，我们可能并不需要这个默认工作表，这个时候我们可以删除这个工作表：

```go
f.DeleteSheet("Sheet1")
```

### 合并单元格

合并 `Sheet1` 工作表上 `F1:I2` 区域内的单元格：

```go
excel.MergeCell("Sheet1", "F1", "I2")
```

### 单元格样式

给单元格设置样式会经常遇到，比如设置单元格的背景颜色，`Excelize` 库提供下面两个方法进行设置单元格样式（`NewStyle` 和 `SetCellStyle`）：

```go
// 通过给定的样式格式 JSON 或结构体的指针创建样式并返回样式索引。
// 请注意，颜色需要使用 RGB 色域代码表示。
style, err := f.NewStyle(`{
    "border": [
    {
        "type": "left",
        "color": "0000FF",
        "style": 3
    },
    {
        "type": "top",
        "color": "00FF00",
        "style": 4
    },
    {
        "type": "bottom",
        "color": "FFFF00",
        "style": 5
    },
    {
        "type": "right",
        "color": "FF0000",
        "style": 6
    },
    {
        "type": "diagonalDown",
        "color": "A020F0",
        "style": 7
    },
    {
        "type": "diagonalUp",
        "color": "A020F0",
        "style": 8
    }]
}`)
if err != nil {
    fmt.Println(err)
}
err = f.SetCellStyle("Sheet1", "D7", "D7", style)
```

#### 文字水平居中

文字水平居中需要用到 `Alignment` 样式结构体：

```go
type Alignment struct {
    Horizontal      string `json:"horizontal"`
    Indent          int    `json:"indent"`
    JustifyLastLine bool   `json:"justify_last_line"`
    ReadingOrder    uint64 `json:"reading_order"`
    RelativeIndent  int    `json:"relative_indent"`
    ShrinkToFit     bool   `json:"shrink_to_fit"`
    TextRotation    int    `json:"text_rotation"`
    Vertical        string `json:"vertical"`
    WrapText        bool   `json:"wrap_text"`
}
```

水平居中只要设置 `Horizontal` 的值为 `center` 即可：

```go
style, err := f.NewStyle(`{"alignment":{"horizontal":"center"}}`)
if err != nil {
    fmt.Println(err)
}
err = excel.SetCellStyle("Sheet1", "B1", "B1", style)
```

#### 给单元格设置纯色填充

给单元格填充颜色会使用到 `Fill` 样式结构体：

```go
type Fill struct {
    Type    string   `json:"type"`
    Pattern int      `json:"pattern"`
    Color   []string `json:"color"`
    Shading int      `json:"shading"`
}
```

#### Style 结构体

从上面设置样式的代码中，我们可以发现 `border` 是一个数组，而 `alignment` 是一个结构体，这是由 `Style` 结构体决定的：

```go
type Style struct {
    Border        []Border    `json:"border"`
    Fill          Fill        `json:"fill"`
    Font          *Font       `json:"font"`
    Alignment     *Alignment  `json:"alignment"`
    Protection    *Protection `json:"protection"`
    NumFmt        int         `json:"number_format"`
    DecimalPlaces int         `json:"decimal_places"`
    CustomNumFmt  *string     `json:"custom_number_format"`
    Lang          string      `json:"lang"`
    NegRed        bool        `json:"negred"`
}
```

## 参考文档

- [Excelize docs reference](https://xuri.me/excelize/)
- [Talks at Beijing Gopher Meetup](https://xuri.me/2019/12/11/talks-at-beijing-gopher-meetup.html)
