---
title: "使用 Golang 玩转 Docker API"
date: 2021-03-28T23:47:10+08:00
weight: 1
aliases: ["/first"]
tags: ["blog"]
categories: ["blog"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://img-blog.csdnimg.cn/20210328234917805.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

Docker 提供了一个与 Docker 守护进程交互的 API (称为Docker Engine API)，我们可以使用官方提供的 Go 语言的 SDK 进行构建和扩展 Docker 应用程序和解决方案。

<!--more-->

## 安装 SDK

通过下面的命令就可以安装 SDK 了：

```bash
go get github.com/docker/docker/client
```

## 管理本地的 Docker

该部分会介绍如何使用 Golang + Docker API 进行管理本地的 Docker。

### 运行容器

第一个例子将展示如何运行容器，相当于 `docker run docker.io/library/alpine echo "hello world"`：

```go
package main

import (
    "context"
    "io"
    "os"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/api/types/container"
    "github.com/docker/docker/client"
    "github.com/docker/docker/pkg/stdcopy"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    reader, err := cli.ImagePull(ctx, "docker.io/library/alpine", types.ImagePullOptions{})
    if err != nil {
        panic(err)
    }
    io.Copy(os.Stdout, reader)

    resp, err := cli.ContainerCreate(ctx, &container.Config{
        Image: "alpine",
        Cmd:   []string{"echo", "hello world"},
    }, nil, nil, "")
    if err != nil {
        panic(err)
    }

    if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
        panic(err)
    }

    statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
    select {
    case err := <-errCh:
        if err != nil {
            panic(err)
        }
    case <-statusCh:
    }

    out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true})
    if err != nil {
        panic(err)
    }

    stdcopy.StdCopy(os.Stdout, os.Stderr, out)
}
```

### 后台运行容器

还可以在后台运行容器，相当于 `docker run -d bfirsh/reticulate-splines`：

```go
package main

import (
    "context"
    "fmt"
    "io"
    "os"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/api/types/container"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    imageName := "bfirsh/reticulate-splines"

    out, err := cli.ImagePull(ctx, imageName, types.ImagePullOptions{})
    if err != nil {
        panic(err)
    }
    io.Copy(os.Stdout, out)

    resp, err := cli.ContainerCreate(ctx, &container.Config{
        Image: imageName,
    }, nil, nil, "")
    if err != nil {
        panic(err)
    }

    if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
        panic(err)
    }

    fmt.Println(resp.ID)
}
```

### 查看容器列表

列出正在运行的容器，就像使用 `docker ps` 一样：

```go
package main

import (
    "context"
    "fmt"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
    if err != nil {
        panic(err)
    }

    for _, container := range containers {
        fmt.Println(container.ID)
    }
}
```

如果是 `docker ps -a`，我们可以通过修改 `types.ContainerListOptions` 中的 `All` 属性达到这个目的：

```go
// type ContainerListOptions struct {
// 	Quiet   bool
// 	Size    bool
// 	All     bool
// 	Latest  bool
// 	Since   string
// 	Before  string
// 	Limit   int
// 	Filters filters.Args
// }

options := types.ContainerListOptions{
    All: true,
}
containers, err := cli.ContainerList(ctx, options)
if err != nil {
    panic(err)
}
```

### 停止所有运行中的容器

通过上面的例子，我们可以获取容器的列表，所以在这个案例中，我们可以去停止所有正在运行的容器。

> 注意：不要在生产服务器上运行下面的代码。

```go
package main

import (
    "context"
    "fmt"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
    if err != nil {
        panic(err)
    }

    for _, container := range containers {
        fmt.Print("Stopping container ", container.ID[:10], "... ")
        if err := cli.ContainerStop(ctx, container.ID, nil); err != nil {
            panic(err)
        }
        fmt.Println("Success")
    }
}
```

### 获取指定容器的日志

通过指定容器的 ID，我们可以获取对应 ID 的容器的日志：

```go
package main

import (
    "context"
    "io"
    "os"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    options := types.ContainerLogsOptions{ShowStdout: true}

    out, err := cli.ContainerLogs(ctx, "f1064a8a4c82", options)
    if err != nil {
        panic(err)
    }

    io.Copy(os.Stdout, out)
}
```

### 查看镜像列表

获取本地所有的镜像，相当于 `docker image ls` 或 `docker images`：

```go
package main

import (
    "context"
    "fmt"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    images, err := cli.ImageList(ctx, types.ImageListOptions{})
    if err != nil {
        panic(err)
    }

    for _, image := range images {
        fmt.Println(image.ID)
    }
}
```

### 拉取镜像

拉取指定镜像，相当于 `docker pull alpine`：

```go
package main

import (
    "context"
    "io"
    "os"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    out, err := cli.ImagePull(ctx, "alpine", types.ImagePullOptions{})
    if err != nil {
        panic(err)
    }

    defer out.Close()

    io.Copy(os.Stdout, out)
}
```

### 拉取私有镜像

除了公开的镜像，我们平时还会用到一些私有镜像，可以是 DockerHub 上私有镜像，也可以是自托管的镜像仓库，比如 [harbor](https://goharbor.io/)。这个时候，我们需要提供对应的凭证才可以拉取镜像。

**值得注意的是：在使用 Docker API 的 Go SDK 时，凭证是以明文的方式进行传输的，所以如果是自建的镜像仓库，请务必使用 `HTTPS`！**

```go
package main

import (
    "context"
    "encoding/base64"
    "encoding/json"
    "io"
    "os"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    authConfig := types.AuthConfig{
        Username: "username",
        Password: "password",
    }
    encodedJSON, err := json.Marshal(authConfig)
    if err != nil {
        panic(err)
    }
    authStr := base64.URLEncoding.EncodeToString(encodedJSON)

    out, err := cli.ImagePull(ctx, "alpine", types.ImagePullOptions{RegistryAuth: authStr})
    if err != nil {
        panic(err)
    }

    defer out.Close()
    io.Copy(os.Stdout, out)
}
```

### 保存容器成镜像

我们可以将一个已有的容器通过 `commit` 保存成一个镜像：

```go
package main

import (
    "context"
    "fmt"

    "github.com/docker/docker/api/types"
    "github.com/docker/docker/api/types/container"
    "github.com/docker/docker/client"
)

func main() {
    ctx := context.Background()
    cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
    if err != nil {
        panic(err)
    }

    createResp, err := cli.ContainerCreate(ctx, &container.Config{
        Image: "alpine",
        Cmd:   []string{"touch", "/helloworld"},
    }, nil, nil, "")
    if err != nil {
        panic(err)
    }

    if err := cli.ContainerStart(ctx, createResp.ID, types.ContainerStartOptions{}); err != nil {
        panic(err)
    }

    statusCh, errCh := cli.ContainerWait(ctx, createResp.ID, container.WaitConditionNotRunning)
    select {
    case err := <-errCh:
        if err != nil {
            panic(err)
        }
    case <-statusCh:
    }

    commitResp, err := cli.ContainerCommit(ctx, createResp.ID, types.ContainerCommitOptions{Reference: "helloworld"})
    if err != nil {
        panic(err)
    }

    fmt.Println(commitResp.ID)
}
```

## 管理远程的 Docker

当然，除了可以管理本地的 Docker， 我们同样也可以通过使用 Golang + Docker API 管理远程的 Docker。

### 远程连接

默认 Docker 是通过非网络的 Unix 套接字运行的，只能够进行本地通信（`/var/run/docker.sock`），是不能够直接远程连接 Docker 的。

我们需要编辑配置文件 `/etc/docker/daemon.json`，并修改以下内容（把 `192.168.59.3` 改成你自己的 IP 地址），然后重启 Docker：

```bash
# vi /etc/docker/daemon.json
{
  "hosts": [
    "tcp://192.168.59.3:2375",
    "unix:///var/run/docker.sock"
  ]
}

systemctl restart docker
```

### 修改 client

创建 `client` 的时候需要指定远程 Docker 的地址，这样就可以像管理本地 Docker 一样管理远程的 Docker 了：

```go
cli, err = client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation(),
    client.WithHost("tcp://192.168.59.3:2375"))
```

## 总结

现在已经有很多可以管理 Docker 的产品，它们便是这样进行实现的，比如：[portainer](https://github.com/portainer/portainer)。
