---
title: "玩转 CICD 之 Groovy 环境配置"
date: 2021-06-02T06:11:40-04:00
weight: 1
aliases: ["/first"]
tags:
- Java
- Groovy
- MacOS
- CentOS
categories:
- Groovy
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://img-blog.csdnimg.cn/20210520122712825.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

最近不是太忙了就是太懒了，反正懒是改不了了，但趁着 [6月更文挑战](https://juejin.cn/post/6967194882926444557) 这个活动多写点吧。该系列文章的主要目标是玩转各种主流平台的 CI/CD，包括但不仅限于 Jenkins/ Drone/ GitHub / GitLab 等现代化自动化工具。

写过 Jenkins Pipeline 的朋友一定知道不论是声明式流水线（Declarative Pipeline）还是脚本式流水线（Scripted Pipeline），其都是基于 Groovy 语法的，所以要想写好 Jenkins Pipeline，掌握基本的 Groovy 语法是必不可少的，那么运行 Groovy 脚本的环境也是不可少的，本文将介绍如何在 CentOS 7 以及 MacOS 上配置 Groovy 的环境。

<!--more-->

## 安装 OpenJDK

安装 groovy 的前置条件是安装 JDK 并配置 `JAVA_HOME` 环境变量。

```bash
# MacOS
brew install openjdk@8
sudo ln -sfn $(brew --prefix)/opt/openjdk@8/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk


# CentOS 7
sudo yum install -y java-1.8.0-openjdk-devel
```

### 配置 Java 的环境变量

groovy 会依赖环境变量 `JAVA_HOME`，编辑 `~/.zshrc` 添加以下内容：

```bashrc
# MacOS
export JAVA_HOME="/Library/Java/JavaVirtualMachines/openjdk.jdk/Contents/Home"
export CLASSPATH="$JAVA_HOME/jre/lib/ext:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar"

# CentOS 7
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.292.b10-1.el7_9.x86_64"
export CLASSPATH="$JAVA_HOME/jre/lib/ext:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar"
```

> Tip: 使用 `which java` 和 `ls -ln $(which java)` 找到 OpenJDK 的安装目录，可能存在多次链接。

## 下载 Groovy 二进制文件

下载地址：https://groovy.apache.org/download.html

```bash
curl -LO https://groovy.jfrog.io/artifactory/dist-release-local/groovy-zips/apache-groovy-binary-3.0.8.zip
unzip apache-groovy-binary-3.0.8.zip
sudo mv groovy-3.0.8 /usr/local
```

## 配置 Groovy 环境变量

编辑 `~/.zshrc` 文件，添加以下内容

```bashrc
export GROOVY_HOME="/usr/local/groovy-3.0.8"
export PATH="$GROOVY_HOME/bin:$PATH"
```

## 验证安装结果

```bash
# 进入命令交互环境
groovysh

# 运行 groovy 脚本
groovy SomeScript
```

## 错误处理

正常安装完成后，在运行 `groovysh` 时会提示下面的错误：

```bash
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/nashorn.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/nashorn.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/jaccess.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/jaccess.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/rt.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/rt.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunec.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunec.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunjce_provider.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunjce_provider.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/cldrdata.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/cldrdata.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/zipfs.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/zipfs.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/localedata.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/localedata.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunpkcs11.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/sunpkcs11.jar (No such file or directory)
WARN [org.apache.groovy.groovysh.util.PackageHelperImpl] Error opening jar file : '/usr/local/Cellar/openjdk@8/1.8.0+282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/dnsns.jar' : java.io.FileNotFoundException: /usr/local/Cellar/openjdk@8/1.8.0 282/libexec/openjdk.jdk/Contents/Home/jre/lib/ext/dnsns.jar (No such file or directory)
Groovy Shell (3.0.8, JVM: 1.8.0_282)
Type ':help' or ':h' for help.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
groovy:000>
```

应该是特殊符号的原因，`+` 被解析成了空格，导致找不到，所以先把这个目录复制一份（`1.8.0+282` -> `1.8.0_282`），然后重新链接：

```bash
sudo cp -r /usr/local/Cellar/openjdk@8/1.8.0+282 /usr/local/Cellar/openjdk@8/1.8.0_282
sudo ln -sfn /usr/local/Cellar/openjdk@8/1.8.0_282 /usr/local/opt/openjdk@8
```

## 参考文档

- [Install Groovy - Install Binary](http://groovy-lang.org/install.html)
