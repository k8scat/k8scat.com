---
title: "更新已经存在的容器"
date: 2020-12-10T03:44:29+08:00
weight: 1
aliases: ["/first"]
tags: ["Docker"]
categories: ["DevOps"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

有一个不知道怎么启动的容器, 由于某些原因会被停掉, 但又不会自动重启,
因为没有设置 `restart policy: always`.

<!--more-->

使用 `docker update` 更新已有容器的配置

```bash
docker update --restart always <container-id>
```
