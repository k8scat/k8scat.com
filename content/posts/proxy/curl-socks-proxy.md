---
title: "[cURL] 指定请求使用的 SOCKS 代理"
date: 2021-09-22T13:56:58+08:00
weight: 1
aliases: ["/first"]
tags: ["curl", "socks"]
categories: ["proxy"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

记录一下 `cURL` 设置 `SOCKS` 代理的方式。

<!--more-->

其中：`<host>` 为主机，`<port>` 为端口，`<url>` 为目标 URL。

## 代理设置方式

### 使用 SOCKS4 代理

```bash
curl --proxy socks4://<host>:<port> <url>
```

### 使用 SOCKS4a 代理

```bash
curl --proxy socks4a://<host>:<port> <url>
```

### 使用 SOCKS5（本地DNS） 代理

```bash
curl --proxy socks5://<host>:<port> <url>
```

### 使用 SOCKS5（远程DNS） 代理

```bash
curl --proxy socks5h://<host>:<port> <url>
```

> 注意协议头是：socks5h://。

## 代理协议的区别

`SOCKS4` 和 `SOCKS4a` 已经被 `SOCKS5` 取代。不应再使用。

Windows IE 的代理貌似只支持到 `SOCKS4a`，不支持 `SOCKS5`。

`SOCKS5` 和 `SOCKS5h` 的区别仅用于 `cURL`。在 `cURL` 中，`SOCKS5` 会使用本地 DNS，`SOCKS5h` 会使用代理提供的远程 DNS。

在其它软件中，`SOCKS5h` 基本不起作用，使用 `SOCKS5` 即可。一旦设置了代理，多数时候会采用远程 DNS。

> 转载自：[https://blog.twofei.com/714/](https://blog.twofei.com/714/)
