---
title: "Git 签名失败: gpg failed to sign the data"
date: 2021-11-22T20:29:06+08:00
weight: 1
aliases: ["/first"]
tags: ["blog"]
categories: ["git"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

在 Goland 里使用 GPG 签名时提示错误：

```plain
error: gpg failed to sign the data
fatal: failed to write commit object
```

<!--more-->

原因居然是 Terminal 的窗口太小了~

```bash
echo "test" | gpg --clearsign
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

test
gpg: signing failed: Screen or window too small
```

## 参考

[gpg failed to sign the data fatal: failed to write commit object [Git 2.10.0]](https://stackoverflow.com/questions/39494631/gpg-failed-to-sign-the-data-fatal-failed-to-write-commit-object-git-2-10-0)
