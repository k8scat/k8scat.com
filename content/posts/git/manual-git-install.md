---
title: "CentOS 7 手动安装指定版本的 Git"
date: 2020-12-22T20:22:07+08:00
weight: 1
aliases: ["/first"]
tags: ["centos", "linux"]
categories: ["git"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/git.png"
    alt: "git"
    caption: "git"
    relative: false
comments: true
---

`yum install git` 默认安装的是比较低的版本，有些选项只有在新版本的 `git` 才支持，比如 `git tag --sort='committerdate'`。

<!--more-->

## 下载 Git 源代码

地址：[https://github.com/git/git/releases](https://github.com/git/git/releases)

## 手动安装

下面是手动安装 Git v2.29.2 的整个流程

```bash
# 安装依赖库
sudo yum install -y make gcc \
    openssl-devel \
    expat-devel \
    curl-devel \
    gettext-devel \
    tcl-devel
# 下载 Git 源码
wget https://github.com/git/git/archive/v2.29.2.tar.gz
tar zxf v2.29.2.tar.gz
cd git-2.29.2
make prefix=/usr
sudo make prefix=/usr install
```

## 一键安装

```bash
curl -sSL https://gist.githubusercontent.com/k8scat/eda787a71d8f327134a0b5508569e451/raw/22f1921a4bd99bd228b0249d9ad4da331d6f03f3/install_git.sh | bash -s 2.29.2
```

## 参考文档

[git/git INSTALL](https://github.com/git/git/blob/master/INSTALL)
