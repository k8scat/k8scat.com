---
title: "使用 Jenkins Pipeline 实现构建和远程部署"
date: 2021-06-04T21:24:27+08:00
weight: 1
aliases: ["/first"]
tags: ["Jenkins Pipeline", "SSH", "Remote Deploy"]
categories: ["Jenkins"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/jenkins-remote-deploy.jpeg"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

本文记录了如何使用 Jenkins Pipeline 实现构建和远程部署。

<!--more-->

## 自动化流程

企业里常见的项目自动化流程应该构建机从代码仓拉取代码进行构建，构建完成后会将产物推送到制品库中，比如镜像仓，
然后中间会有测试环境，用于进行自动化测试或人工测试，最后进行远程部署。

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/enterprise-project-pipeline.jpg)

## 项目结构

这里我们用的 Go 的项目结构，它大概的结构应该是下面这样的：

```
|-- my-app
    |-- .gitignore
    |-- README.md
    |-- LICENSE
    |-- go.mod
    |-- go.sum
    |-- main.go
    |-- pkg
        |-- ...
```

## 项目构建

因为这里构建的是 Go 的项目，如果用到私有库，在 `go mod tidy` 时会要求提供 Git 凭证，我们可以现在 Jenkins 的凭证管理中创建 `Username with password` 类型的凭证，其中 `Username` 就是 GitHub 的用户名，`password` 则是 GitHub 的 `AccessToken`，这里主要用到的是 `AccessToken`，`Username` 其实并不需要。

但在 Jenkins Pipeline 中使用 `usernamePassword` 时要求同时定义用户名变量名 `usernameVariable` 和 密码变量名 `passwordVariable`。

```groovy
stage('Build') {
    steps {
        withCredentials(bindings: [
            usernamePassword(credentialsId: 'GITHUB_CREDENTIAL',
                usernameVariable: 'GITHUB_USER',
                passwordVariable: 'GITHUB_ACCESS_TOKEN'
            )
        ]) {
            sh '''
            git config --global url."https://${GITHUB_ACCESS_TOKEN}:x-oauth-basic@github.com/".insteadOf "https://github.com/"
            
            go mod tidy
            go build -o bin/my-app main.go
            '''
        }
    }
}
```

## 远程部署

在构建完成后，我们会将构建产物推送到制品库，然后我们可以从制品库中拉取构建产物进行部署测试环境并进行测试，在验证通过后，会从制品库中拉取验证通过的产物进行部署上线。

但在本文中，我们的应用相对简单，可以忽略推送产物到制品库以及中间的测试验证环节，目标是实现构建后立即部署上线。

一般来说，线上环境和构建环境不会是同一台机器，所以这个时候我们需要将构建产物复制到另一台服务器上，然后在另一台服务器上进行部署。

由于需要对另一台服务器进行操作，所以我们需要在 Jenkins 上配置 `DEPLOY_HOST`、`DEPLOY_PORT` 和 `SSH_CREDENTIAL` 三个凭证，其中 `DEPLOY_HOST` 和 `DEPLOY_PORT` 是 `Secret text` 类型的凭证，`SSH_CREDENTIAL` 是 `SSH Username with private key` 类型的凭证。

```groovy
stage('Deploy') {
    environment {
        DEPLOY_HOST = credentials('DEPLOY_HOST')
        DEPLOY_PORT = credentials('DEPLOY_PORT')
    }
    steps {
        withCredentials([
            sshUserPrivateKey(credentialsId: 'SSH_CREDENTIAL',
                keyFileVariable: 'SSH_KEY',
                usernameVariable: 'SSH_USERNAME'),
        ]) {
            sh """
            mkdir -p ~/.ssh && chmod 700 ~/.ssh
            echo 'StrictHostKeyChecking no' >> /etc/ssh/ssh_config
            cat ${SSH_KEY} > ~/.ssh/id_rsa && chmod 400 ~/.ssh/id_rsa

            scp -P ${DEPLOY_PORT} bin/my-app ${SSH_USER}@${DEPLOY_HOST}:/data/my-app
            ssh -p ${DEPLOY_PORT} ${SSH_USER}@${DEPLOY_HOST} \"nohup /data/my-app >> /data/my-app.log 2>&1 &\"
            """
        }
    }
}
```

部署的步骤主要包括：

1. 复制构建产物到部署服务器
2. 在部署服务器上执行部署命令，比如 `nohup /data/my-app >> /data/my-app.log 2>&1 &`

其中简化了一些细节，比如在部署前，我们需要先备份数据。所以这里我们可以写一个复杂的部署脚本 `deploy.sh` 放在项目中，然后在 Jenkins Pipeline 中使用 `scp` 将部署脚本文件复制到部署服务器，假设放在 `/data/deploy.sh`，最后只需 `ssh -p ${DEPLOY_PORT} ${SSH_USER}@${DEPLOY_HOST} /bin/bash /data/deploy.sh` 即可。

### 关于 `SSH Username with private key` 类型的凭证

上面的脚本里面，我们在使用 `withCredentials` 加载 `sshUserPrivateKey` 之后，
直接使用 `cat ${SSH_KEY} > ~/.ssh/id_rsa` 将私钥的内容写到 `~/.ssh/id_rsa` 文件中，
表明此类型的凭证会将私钥存储在文件里，而且在我验证后发现确实如此，
私钥内容会存在 `/var/jenkins_home/workspace/test@tmp/secretFiles/3ccd3d00-05fa-412d-951d-b4e12918d8c6/ssh-key-SSH_KEY` 这个文件下，
其中 `test@tmp` 中的 `test` 是我测试用的 Job 名称，后面的 `tmp` 表明这是 Job 的一个临时工作目录，
真正的工作目录应该是 `/var/jenkins_home/workspace/test`。

验证使用 Jenkins Pipeline 脚本如下：

```groovy
pipeline {
    agent {
        label 'master'
    }
    
    stages {
        stage('test') {
            steps {
                withCredentials(
                    bindings: [
                        sshUserPrivateKey(credentialsId: 'SSH_CREDENTIAL',
                        keyFileVariable: 'SSH_KEY',
                        usernameVariable: 'SSH_USERNAME')
                    ]
                ) {
                    sh '''
                    echo ${SSH_KEY} > /tmp/privkey_env
                    cat ${SSH_KEY} > /tmp/privkey_content
                    '''
                }
            }
        }
    }
}
```

新建一个测试用的 Pipeline Job，将上面的内容作为 Pipeline 脚本，构建完成后，
就可以前往 Jenkins 所在的宿主机（如果是使用容器部署的，就进入到 Jenkins 的容器内），
分别查看 `/tmp/privkey_env` 和 `/tmp/privkey_content` 的内容，第一个文件表明私钥内容被保存在哪个文件里，
第二个则是私钥的具体内容。

## 附完整的 Jenkins Pipeline

```groovy
pipeline {
    agent {
        docker {
            image 'golang:1.15-alpine'
            args '-v /data/my-app-cache:/go/.cache'
        }
    }

    options {
        timeout(time: 20, unit: 'MINUTES')
        disableConcurrentBuilds()
    }

    stages {
        stage('Build') {
            steps {
                withCredentials(bindings: [
                    usernamePassword(credentialsId: 'GITHUB_CREDENTIAL',
                        usernameVariable: 'GITHUB_USER',
                        passwordVariable: 'GITHUB_ACCESS_TOKEN'
                    )
                ]) {
                    sh '''
                    git config --global url."https://${GITHUB_ACCESS_TOKEN}:x-oauth-basic@github.com/".insteadOf "https://github.com/"
                    
                    go mod tidy
                    go build -o bin/my-app main.go
                    '''
                }
            }
        }

        stage('Deploy') {
            environment {
                DEPLOY_HOST = credentials('DEPLOY_HOST')
                DEPLOY_PORT = credentials('DEPLOY_PORT')
            }
            steps {
                withCredentials([
                    sshUserPrivateKey(credentialsId: 'SSH_CREDENTIAL',
                        keyFileVariable: 'SSH_KEY',
                        usernameVariable: 'SSH_USERNAME'),
                ]) {
                    sh """
                    mkdir -p ~/.ssh && chmod 700 ~/.ssh
                    echo 'StrictHostKeyChecking no' >> /etc/ssh/ssh_config
                    cat ${SSH_KEY} > ~/.ssh/id_rsa && chmod 400 ~/.ssh/id_rsa

                    scp -P ${DEPLOY_PORT} bin/my-app ${SSH_USER}@${DEPLOY_HOST}:/data/my-app
                    ssh -p ${DEPLOY_PORT} ${SSH_USER}@${DEPLOY_HOST} \"nohup /data/my-app >> /data/my-app.log 2>&1 &\"
                    """
                }
            }
        }
    }
}
```

## 感谢阅读

笔者不才，非常欢迎大家对本文的内容进行指正，或者与笔者进行探讨！
