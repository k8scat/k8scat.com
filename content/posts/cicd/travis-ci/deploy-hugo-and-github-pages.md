---
title: "使用 Travis CI 自动部署基于 Hugo + GitHub Pages 的静态博客"
date: 2021-06-14T23:13:11+08:00
weight: 1
aliases: ["/first"]
tags: ["hugo", "Travis CI", "GitHub Pages", "blog"]
categories: ["CI/CD", "Travis CI"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/travis-ci-github.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

Travis CI 是一个持续集成工具，它可以通过 GitHub 自动拉取代码，然后执行测试、构建以及部署。本文的目标是使用 Travis CI 自动部署基于 Hugo 生成的静态博客到 GitHub Pages 上。

<!--more-->

## 基于 Hugo 的静态博客

[Hugo](https://gohugo.io/) 是最流行的开源静态站点生成器之一。

### 安装 Hugo

在 Linux 上安装 Hugo 是最简单的了，因为只要下载解压就可以用了：

```bash
curl -LO https://github.com/gohugoio/hugo/releases/download/v0.83.1/hugo_0.83.1_Linux-64bit.tar.gz
sudo tar -C /usr/local/bin -zxf hugo_0.83.1_Linux-64bit.tar.gz
```

### 创建新的站点

创建一个新的站点只需一行命令，比如我们这里创建 [fastcicd.com](https://fastcicd.com)：

```bash
hugo new site fastcicd.com
```

### 使用主题

Hugo 有很多主题，其中很多都是开源的，我们可以选择自己喜欢的，比如这里我们使用 [LoveIt](https://github.com/dillonzq/LoveIt) 主题：

```bash
cd fastcicd.com
git init
git submodule add https://github.com/dillonzq/LoveIt.git themes/LoveIt
```

然后修改站点的配置文件 `config.toml`，比如下面这样：

```toml
baseURL = "https://fastcicd.com/"
# [en, zh-cn, fr, ...] determines default content language
defaultContentLanguage = "en"
# language code
languageCode = "en"
title = "My New Hugo Site"

# Change the default theme to be use when building the site with Hugo
theme = "LoveIt"

[params]
  # LoveIt theme version
  version = "0.2.X"

[menu]
  [[menu.main]]
    identifier = "posts"
    # you can add extra information before the name (HTML format is supported), such as icons
    pre = ""
    # you can add extra information after the name (HTML format is supported), such as icons
    post = ""
    name = "Posts"
    url = "/posts/"
    # title will be shown when you hover on this menu link
    title = ""
    weight = 1
  [[menu.main]]
    identifier = "tags"
    pre = ""
    post = ""
    name = "Tags"
    url = "/tags/"
    title = ""
    weight = 2
  [[menu.main]]
    identifier = "categories"
    pre = ""
    post = ""
    name = "Categories"
    url = "/categories/"
    title = ""
    weight = 3

# Markup related configuration in Hugo
[markup]
  # Syntax Highlighting (https://gohugo.io/content-management/syntax-highlighting)
  [markup.highlight]
    # false is a necessary configuration (https://github.com/dillonzq/LoveIt/issues/158)
    noClasses = false
```

### 新建文章

新建文章同样是一行命令搞定：

```bash
hugo new posts/first_post.md
```

### 本地启动

使用下面的命令就可以在本地运行（上面新建的文章内容不能为空，否则会运行失败）：

```bash
hugo serve
```

## Travis CI 自动部署

### GitHub 代码仓

首先，我们需要在 GitHub 上面新建一个代码仓，比如 [k8scat/fastcicd.com](https://github.com/k8scat/fastcicd.com)。

### Travis CI 配置

创建完代码仓后，我们先不急着推送代码，我们需要在项目的根目录下添加一个 `.travis.yml` 文件，内容如下：

```yaml
dist: xenial # 使用 Ubuntu xenial

before_install:
  - sudo apt-get update
  - sudo apt-get -y install curl

# 安装 hugo
install:
  - curl -LO https://github.com/gohugoio/hugo/releases/download/v0.83.1/hugo_0.83.1_Linux-64bit.tar.gz
  - sudo tar -C /usr/local/bin -zxf hugo_0.83.1_Linux-64bit.tar.gz

# 更新主题并生成静态博客
script:
  - git submodule update --init --recursive
  - git submodule update --remote --merge
  - hugo --buildDrafts --gc --verbose --minify

# 部署到 GitHub Pages
deploy:
  provider: pages
  skip_cleanup: true
  github_token: $GITHUB_TOKEN
  keep_history: true
  on:
    branch: master # 仅对 master 分支进行部署 GitHub Pages
  local_dir: public # 生成静态博客的目录
  target_branch: pages # 部署 GitHub Pages 的分支
```

### 添加环境变量

在部署到 GitHub Pages 时，我们需要用到 GitHub Personal AccessToken，这种敏感的配置我们都不应该直接以明文的方式写在配置文件里，所以我们使用 `$GITHUB_TOKEN` 环境变量的形式，即需要在 Travis CI 指定的代码仓中进行配置环境变量：

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/travis-ci-environment-variables.png)

### 自动部署

最后，我们只要将代码推送到 GitHub 代码仓，就可以自动部署静态博客到 GitHub Pages 上了：

```bash
git add .
git commit -m "init"
git push origin master
```

## 项目地址

[k8scat/fastcicd.com](https://github.com/k8scat/fastcicd.com)
