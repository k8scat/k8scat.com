---
title: Resolve Rust Analyzer Server Start Failed
date: "2021-05-31T14:32:50+08:00"
weight: 1
aliases:
- /first
tags:
- rust-analyzer
categories:
- rust
author: K8sCat
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
  image: <image path/url>
  alt: <alt text>
  caption: <text>
  relative: false
comments: true
juejin:
  title: 解决 VSCode Rust Analyzer Server 启动失败的问题
  tags:
  - Rust
  - Visual Studio Code
  category: 后端
  cover_image: https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/a1de961a3689462c949e4d2597605b52~tplv-k3u1fbpfcp-watermark.image?
  brief_content: 在 Rust 论坛上经常碰到的问题是 Rust 用什么编辑器或 IDE 好，如果是一年前，回答可能很多，但就现在而言，我觉得使用
    VSCode 配合 rust-analyzer(以下简称RA) 是最好的组合。
  prefix_content: 'PS: Gopher into Rustacean.'
  suffix_content: |
    # 自动发布

    本文由 `Articli` 工具自动发布，原文链接：[Resolve Rust Analyzer Server Start Failed]([https://](https://k8scat.com/posts/rust/resolve-rust-analyzer-server-start-failed/))。
  draft_create_time: "2022-01-23 15:21:01"
  draft_id: "7056297108730544164"
  article_create_time: "2022-01-23 15:35:19"
  article_id: "7056301144888328199"
---

<!--more-->

## Bootstrap err

VSCode extension `rust-analyzer` shows follow errors:

```
ERROR [8/17/2020, 12:47:15 PM]: Bootstrap error [Error: Failed to execute /home/k8scat/.vscode-server/data/User/globalStorage/matklad.rust-analyzer/rust-analyzer-linux --version
	at bootstrapServer (/home/k8scat/.vscode-server/extensions/matklad.rust-analyzer-0.2.281/out/src/main.js:16169:15)
	at async bootstrap (/home/k8scat/.vscode-server/extensions/matklad.rust-analyzer-0.2.281/out/src/main.js:16112:18)
	at async tryActivate (/home/k8scat/.vscode-server/extensions/matklad.rust-analyzer-0.2.281/out/src/main.js:16039:24)
	at async activate (/home/k8scat/.vscode-server/extensions/matklad.rust-analyzer-0.2.281/out/src/main.js:16013:5)
	at async Promise.all (index 0)]
```

## Resolved

Execute the command in shell

```bash
$ /home/k8scat/.vscode-server/data/User/globalStorage/matklad.rust-analyzer/rust-analyzer-linux --version

# Output
/home/bheisler/.vscode-server/data/User/globalStorage/matklad.rust-analyzer/rust-analyzer-linux: /lib64/libc.so.6: version `GLIBC_2.18' not found (required by /home/bheisler/.vscode-server/data/User/globalStorage/matklad.rust-analyzer/rust-analyzer-linux)
```

So we can know that need to install some libs to resolve this problem.

```bash
wget http://mirrors.ustc.edu.cn/gnu/libc/glibc-2.18.tar.gz
tar zxf glibc-2.18.tar.gz

mkdir build
cd build

../configure --prefix=/usr
make -j4
sudo make install
```
