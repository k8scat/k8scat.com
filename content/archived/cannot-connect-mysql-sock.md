---
title: Django 无法连接本地 MySQL
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.GrandCanyonEast_ZH-CN2721062078_UHD.jpg'
date: 2019-10-09 21:23:42
tags:
- mysql.sock
categories:
- Django
---

本地用 `Docker` 跑了一个 `MySQL`, 使用 `Django` 连接 `MySQL` 报错:
`django.db.utils.OperationalError: (2002, "Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)")`

<!-- more -->

只要将 `localshot` 改为 `127.0.0.1` 即可

开始的配置:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'database_name',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

```

修改后的配置:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'database_name',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

```

