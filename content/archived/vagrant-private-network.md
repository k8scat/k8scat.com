---
title: Vagrant 配置私有网络
comments: true
date: 2019-07-02 22:54:26
tags:
- private_network
categories:
- Vagrant
thumbnail: https://source.unsplash.com/8d8KfpYCQV0
---

`Vagrant` 如何配置私有网络?

<!-- more -->

```
$ vagrant suspend
==> default: Saving VM state and suspending execution...

$ vi Vagrantfile
config.vm.network "private_network", ip: "192.168.33.10"

$ vagrant reload

$ ping 192.168.33.10
PING 192.168.33.10 (192.168.33.10): 56 data bytes
64 bytes from 192.168.33.10: icmp_seq=0 ttl=64 time=0.430 ms
64 bytes from 192.168.33.10: icmp_seq=1 ttl=64 time=0.408 ms
64 bytes from 192.168.33.10: icmp_seq=2 ttl=64 time=0.389 ms
64 bytes from 192.168.33.10: icmp_seq=3 ttl=64 time=0.352 ms
^C
--- 192.168.33.10 ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.352/0.395/0.430/0.029 ms

```

Refer: [第二节：Private Networks（私有网络）](https://www.kancloud.cn/louis1986/vagrant/517615)