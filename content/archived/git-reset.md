---
title: Git 必备技能 - 版本回退
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.Wachsenburg_ZH-CN5224299503_1920x1080.jpg'
date: 2019-09-16 10:23:23
tags:
- reset
categories:
- Git
---

线上版本有bug, 想回退到之前的版本怎么办?

<!-- more -->

```bash
$ git log --pretty=oneline
1094adb7b9b3807259d8cb349e7df1d4d6477073 (HEAD -> master) append GPL
e475afc93c209a690c39c13a46716e8fa000c366 add distributed
eaadf4e385e865d25c48e7ca9c8395c3f7dfaef0 wrote a readme file
# 回退到上一个版本
$ git reset --hard HEAD^
# 回退到指定版本
$ git reset --hard eaadf4e385e86

```

参考: https://www.liaoxuefeng.com/wiki/896043488029600/897013573512192

