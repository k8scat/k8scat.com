---
title: Redis入门之安装
comments: true
date: 2019-06-22 21:10:07
tags:
- redis
- install
categories:
- redis
thumbnail: https://source.unsplash.com/BMYQaySauY0
toc: true
---

<!-- more -->

## 手动安装

```
# 在centos7上进行安装
wget http://download.redis.io/releases/redis-5.0.5.tar.gz
tar xzf redis-5.0.5.tar.gz
cd redis-5.0.5
make MALLOC=libc # https://github.com/antirez/redis#allocator

./src/redis-server ./redis.conf # 在 ./redis.conf 中设置 daemonize yes 以守护进程的形式启动

./src/redis-cli shutdown # 停止 redis-server

./src/redis-cli # 连接 redis-server
127.0.0.1:6379> ping
PONG # 说明连接成功

```

## Docker安装

```
docker run --name redis -d -p 6379:6379 redis:5

docker exec -it redis /bin/bash # 进入容器

./redis-cli # 连接redis-server
127.0.0.1:6379> ping
PONG
```


