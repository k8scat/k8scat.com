---
title: Linux 给用户添加 sudo 权限
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.NaranjoBulnes_ZH-CN0696600867_UHD.jpg'
date: 2019-10-29 20:46:56
tags:
- sudoers
- linux
categories:
- DevOps
---

`CentOS7` 上新建一个用户(`useradd test`), 执行命令 `sudo yum update`,
提示 `test 不在 sudoers 文件中。此事将被报告。`, 那就添加吧!

<!-- more -->

Q: 什么是 `sudo` ? 有什么用?

A: Sudoers allows particular users to run various commands as the root user, without needing the root password.

```bash
# 切换到root或者可以执行sudo命令的用户下, 然后编辑 /etc/sudoers
sudo vi /etc/sudoers

# 添加以下内容(需要使用 :wq! 强制保存)
test   ALL=(ALL)       ALL

```
