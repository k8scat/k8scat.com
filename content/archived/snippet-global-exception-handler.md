---
title: SpringBoot 接口异常统一处理
comments: true
thumbnail: 'https://source.unsplash.com/7jZNgIuJrCM'
toc: true
date: 2019-08-13 09:38:36
tags:
- GlobalExceptionHandler
categories:
- Java
---

当接口出现未知异常时, 如何返回统一的信息?

<!-- more -->

Response.java:

```java

package com.ncucoder.collector.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="https://hsowan.me">hsowan</a>
 **/
@Data
@NoArgsConstructor
public class Response {
    private Integer code;
    private Object body;

    public Response(Integer code, Object body) {
        this.code = code;
        this.body = body;
    }
}

```

GlobalExceptionHandler.java:

```java

package com.ncucoder.collector.advice;

import com.ncucoder.collector.vo.Response;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * @author <a href="https://hsowan.me">hsowan</a>
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handler(Exception e) {
        return new Response(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
    }

}


```
