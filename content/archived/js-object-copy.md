---
title: js 对象拷贝
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.VancouverFall_ZH-CN9824386829_1920x1080.jpg'
date: 2019-09-22 23:28:39
tags:
- 对象拷贝
categories:
- JavaScript
---

今天写 `Vue` 前端项目的时候遇到这样一个问题(但这个问题和 `Vue` 似乎没有半毛钱关系), 就是存在一个对象, 怎样才能不改变这个原有的对象进行操作?

<!-- more -->

很明显, 简单的 `let newObj = oldObj` 是肯定不行的, 可以通过 `浅拷贝` 进行解决:
```js
let oldObj = {
	a: 1,
	b: '2'
}

let newObj = Object.assign({}, oldObj)

```

参考:
* https://juejin.im/entry/5a28ec86f265da43163cf720
* https://www.cnblogs.com/linhp/p/6085826.html