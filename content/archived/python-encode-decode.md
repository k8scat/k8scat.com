---
title: 今天又被字符编码虐了, 我太难了
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.MountFanjing_ZH-CN1999613800_UHD.jpg'
toc: true
date: 2019-09-04 17:25:18
tags:
- 字符编码
- encode
- decode
categories:
- python
---

```python
'\\u4eca\\u5929\\u53c8\\u88ab\\u5b57' \
'\\u7b26\\u7f16\\u7801\\u8650\\u4e86' \
'\\u002c\\u0020\\u6211\\u592a\\u96be\\u4e86'\
    .encode('utf-8').decode('unicode_escape')
```

<!-- more -->

Talk is cheap, show you the code.

```python
print(response.body)
> b'{"11665732":{"id":"11665732","title":"\\u524d\\u7aef\\u9879\\u76ee-snabbdom.zip","tag":"\\u524d\\u7aef\\u9879\\u76ee","categoryid":"16014","sourcescore":"5","username":"weixin_38743737","sourcesize":"303757","pubdate":"2019-09-04","description":"\\u524d\\u7aef\\u9879\\u76ee-snabbdom,\\u4e00\\u4e2a\\u6ce8\\u91cd\\u7b80\\u5355\\u6027\\u3001\\u6a21\\u5757\\u5316\\u3001\\u5f3a\\u5927\\u529f\\u80fd\\u548c\\u6027\\u80fd\\u7684\\u865a\\u62dfDOM\\u5e93\\u3002","filetype":".zip","fileaddr":"\\/20190904\\/34470584265A4CF68BC092C8437FD084-\\u524d\\u7aef\\u9879\\u76ee-snabbdom.zip","download_num":null,"sourcetype":"2","operator":"zzsj666","pubdate_int":"1567588978","is_hot":false,"image":"\\/\\/csdnimg.cn\\/release\\/download\\/old_static\\/images\\/minetype\\/unkonw.svg","nickname":"weixin_38743737"}}'

print(response.encoding)
> utf-8

print(chardet.detect(response.body))
> {'encoding': 'ascii', 'confidence': 1.0, 'language': ''}

print(response.body.decode('unicode_escape'))
> {"11665732":{"id":"11665732","title":"前端项目-snabbdom.zip","tag":"前端项目","categoryid":"16014","sourcescore":"5","username":"weixin_38743737","sourcesize":"303757","pubdate":"2019-09-04","descr"前端项目-snabbdom,一个注重简单性、模块化、强大功能和性能的虚拟DOM库。","filetype":".zip","fileaddr":"\/20190904\/34470584265A4CF68BC092C8437FD084-前端项目-snabbdom.zip","download_num":null,"sourcepubdate_int":"1567588978","is_hot":false,"image":"\/\/csdnimg.cn\/release\/download\/old_static\/images\/minetype\/unkonw.svg","nickname":"weixin_38743737"}}

```

```python
# 上述解决方案存在问题, json.loads时会出现错误
# 最终解决方案
json.loads(response.body.decode('utf-8'))

```

参考项目: [CSDNHub](https://github.com/hsowan/CSDNHub)



