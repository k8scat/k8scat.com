---
title: 使用 scp 下载远程服务器的文件 
comments: true
thumbnail: 'https://source.unsplash.com/jGaORUJARco'
date: 2019-08-23 05:37:11
tags:
- scp
categories:
- DevOps
---

<!-- more -->

```sh
# 将远程服务器上的 a.txt 文件下载到本地 /data/ 下
# -P 指定远程服务器的端口
scp -P 22 root@remote_host:/a.txt /data/

```
