---
title: SpringBoot + Maven 多环境配置
comments: true
date: 2019-07-28 01:23:12
tags:
- SpringBoot
- 多环境配置
- Maven
categories:
- SpringBoot
thumbnail: https://source.unsplash.com/9270-pFGVTU
toc: true
---

一般来说, 一个项目会有多个环境, 比如生产环境(prod), 开发环境(dev), 测试环境(test), 这些环境的配置肯定会有所不同, 那么如何使用 `Maven` + `SpringBoot` 进行多环境配置?

<!-- more -->

1. 在 `resources` 下有以下四个文件, 其中 `application.yml` 是基础配置文件, 其他配置文件的格式必须是 `application-*.xml`:
   
* application.yml

```yml
spring:
  profiles:
    active: dev, prod, test

```

* application-dev.yml

```yml
server:
  port: 8000

```

* application-prod.yml

```yml
server:
  port: 9000

```

* application-test.yml

```yml
server:
  port: 9996

```

2. 在 `pom.xml` 文件添加以下内容:

```xml
<profiles>
    <profile>
        <id>dev</id>
        <properties>
            <active>dev</active>
        </properties>
        <activation>
            <!-- 默认启用的环境 -->
            <activeByDefault>true</activeByDefault>
        </activation>
    </profile>
    <profile>
        <id>prod</id>
        <properties>
            <active>prod</active>
        </properties>
    </profile>
    <profile>
        <id>test</id>
        <properties>
            <active>test</active>
        </properties>
    </profile>
</profiles>

<build>
    <!-- 管理资源文件 -->
    <resources>
        <resource>
            <directory>${basedir}/src/main/resources</directory>
            <excludes>
                <!-- 过滤所有配置文件 -->
                <exclude>**/application-*.yml</exclude>
            </excludes>
        </resource>
        <resource>
            <filtering>true</filtering>
            <directory>${basedir}/src/main/resources</directory>
            <includes>
                <!-- 加载特定环境的配置文件 -->
                <include>**/application-${active}.yml</include>
            </includes>
        </resource>
    </resources>
</build>

```

3. 在不同环境中启动项目:

```shell
# dev
mvn spring-boot:run

# prod
mvn spring-boot:run -Pprod

# test
mvn spring-boot:run -Ptest

```

如果尝试使用类似 `@active@` 或者 `'@active@'` 进行多环境配置, 可能会出现 `yml` 的解析问题(以前是可以的: [SpringBoot-demo](https://github.com/hsowan/SpringBoot-demo)),
其实是没有必要这样做的, 因为 `SpringBoot` 是支持同时启用多个环境的, 比如:

```yml
spring:
  profiles:
    active: dev, prod, test

```

然后通过 `Maven` 对资源文件进行管理即可