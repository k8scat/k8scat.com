---
title: pip 配置源
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.AlbertaThanksgiving_ZH-CN5899007960_UHD.jpg'
date: 2019-10-20 13:07:43
tags:
- pip
- config
categories:
- python
---

`pip` 可以通过配置国内源加快安装依赖的速度, 可以直接舔加 `-i` 参数,
但是每次安装依赖都要添加很麻烦, 所以可以通过修改配置文件, 这样以后安装依赖就不需要添加了,但不同的系统的配置文件所在的位置不同(Mac: `~/.config/pip/pip.conf`, Windows: `~/AppData/Roaming/pip/pip.ini`),
可以通过 `pip config` 达到不同系统下修改配置的目的.

<!-- more -->

### pip 国内的一些镜像

+ 阿里云 https://mirrors.aliyun.com/pypi/simple/
+ 中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/
+ 豆瓣(douban) http://pypi.douban.com/simple/
+ 清华大学 https://pypi.tuna.tsinghua.edu.cn/simple/
+ 中国科学技术大学 http://pypi.mirrors.ustc.edu.cn/simple/

修改源方法:

临时使用:
可以在使用pip的时候在后面加上-i参数，指定pip源
eg:	pip install scrapy -i https://pypi.tuna.tsinghua.edu.cn/simple

### 配置文件

##### Linux
修改 ~/.pip/pip.conf (没有就创建一个)， 内容如下：

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host=pypi.tuna.tsinghua.edu.cn
```

##### Windows:
直接在user目录中创建一个pip目录，如：C:\Users\xx\pip，新建文件pip.ini，内容如下

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host=pypi.tuna.tsinghua.edu.cn
```

### 命令行方式

```bash
pip config set global.index-url https://pypi.mirrors.ustc.edu.cn/simple/

```
