---
title: shell 写单行或多行内容至文件
comments: true
thumbnail: 'https://source.unsplash.com/LD8FhvuLiEA'
date: 2019-08-21 09:00:17
tags:
- echo
- cat
categories:
- shell
---

如何使用 `shell` 将单行或多行内容写至文件?

<!-- more -->

单行内容

```sh
echo 'content' > file

```

多行内容

```sh

cat > file << EOF
hello
world
!!!
EOF

tee file <<-'EOF'
a
b
c
EOF

```