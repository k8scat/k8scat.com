---
title: 使用 rsync 迁移数据
comments: true
thumbnail: 'https://source.unsplash.com/mV4GwTDD8Tw'
date: 2019-08-22 16:01:36
tags:
- rsync
- scp
categories:
- DevOps
---

<!-- more -->

```sh
# 在 A 主机上执行下面的命令
# 将 A 主机上的 /data/ 下的所有文件迁移到 B 主机上 /data/ 下
# ssh -p 22 可以指定 B 主机的端口
# --progress 显示迁移进度
rsync -av --progress /data/ -e 'ssh -p 22' username@hostname:/data/

```
