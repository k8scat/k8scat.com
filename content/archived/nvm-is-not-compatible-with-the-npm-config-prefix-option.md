---
title: nvm is not compatible with the npm config “prefix” option
comments: true
date: 2019-06-18 07:35:40
tags:
- nvm
- terminal
- node
- npm
categories:
thumbnail: https://source.unsplash.com/ubY-igG0jVo
toc: true
---

<!-- more -->

```
npm config delete prefix
npm config set prefix $NVM_DIR/versions/node/`nvm current`
```

https://stackoverflow.com/questions/34718528/nvm-is-not-compatible-with-the-npm-config-prefix-option