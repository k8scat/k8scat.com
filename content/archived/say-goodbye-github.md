---
title: 再见, GitHub
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.ClavijoLandscape_ZH-CN1525245124_1920x1080.jpg'
date: 2019-09-29 23:17:43
tags:
- GitHub
categories:
- git
---

“我送你离开千里之外你无声黑白” -- 周杰伦《千里之外》

<!-- more -->

由于自己写的一个 [项目](https://gitee.com/hsowan/CSDNBot) 在线上通过 `GitHub API` 获取 `star`,
真的是没有任何预兆, 被 `GitHub` 官方封号了, 我很难过, 也很后悔, 有一种 生死掌握在别人手中 的感觉,
所以才会有那么多开源的可自托管的Git平台, 比如 `GitLab`, `Gogs`, `Gitea`,
还是用自己托管的Git平台靠谱, 再见, GitHub!

{% img http://qiniu.ncucoder.com/github-suspended.png %}