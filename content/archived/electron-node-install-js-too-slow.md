---
title: 无法安装 Electron
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.BarcolanaTrieste_ZH-CN5745744257_1920x1080.jpg'
date: 2019-10-12 08:28:35
tags:
- Electron
categories:
- npm
---

安装 `Electron` 的时候一直卡在 `node install.js ... and 1 more`

<!-- more -->

通过配置 `electron_mirror` 即可解决:

```
vi ~/.npmrc

# 添加以下内容
electron_mirror="https://npm.taobao.org/mirrors/electron/"

```