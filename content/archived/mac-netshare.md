---
title: Mac 互联网共享 -- WIFI
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.TinternAbbey_ZH-CN1922933358_1920x1080.jpg'
date: 2019-10-05 10:12:03
tags:
- wifi
categories:
- Mac
---

Mac 互联网共享时, 安卓手机无法扫描到网络, 如何解决?

<!-- more -->

具体操作:

1. 鼠标点击左上角，选择“系统偏好设置”，点击弹出界面的“共享”；
2. 点击“互联网共享”，先不要打勾选中，。来源选择以太网或者PPPOE等已经联网的方式；
3. 使用以下设备共享网络出选择“WiFi”，点击左下角的“WiFi选项”，填好WiFi名称，频段选择11或者48，安全性建议设置密码；
都完成后，在“互联网共享”前的方框上打勾，即可共享网络

转载: https://www.cnblogs.com/rison13/p/5518404.html
