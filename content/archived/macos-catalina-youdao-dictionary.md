---
title: 有道词典 for Catalina
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.ChandraTal_ZH-CN2809744505_1920x1080.jpg'
date: 2019-11-03 14:36:16
tags:
- 有道词典
- 闪退
categories:
- macOS
---

macOS 更新 Catalina 之后有道词典闪退的解决办法

<!-- more -->

打开系统偏好设置 => 语言与地区 => App => 点击+号 => 选择有道词典,语言设置成英语
