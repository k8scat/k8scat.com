---
title: WebStorm 提示 iView 标签错误
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.CrimsonRosella_ZH-CN1581892400_1920x1080.jpg'
date: 2019-09-30 01:10:53
tags:
- WebStorm
- iView
categories:
- frontend
---

用 `WebStorm` 写 [iView](https://www.iviewui.com/) 项目时, 满屏报红, 提示: `ESLint: Parsing error: x-invalid-end-tag.(vue/no-parsing-error)`, 如何解决? 

<!-- more -->

在 `eslintrc.js` 中添加下面内容:

```js
rules: {
    "vue/no-parsing-error": [2, {
        "x-invalid-end-tag": false
    }]
}

```

参考: https://github.com/iview/iview/issues/2828
