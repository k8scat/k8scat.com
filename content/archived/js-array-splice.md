---
title: JavaScript Array.splice()
comments: true
date: 2019-06-24 12:00:27
tags:
- Array
- splice
categories:
- JavaScript
thumbnail: https://source.unsplash.com/3b4qppTryQ0
toc: true
---

<!-- more -->

```js
// Array.splice(start[, deleteCount[, item1[, item2[, ...]]]])
// 第一个参数是开始的下标
// 第二个参数是从start下标开始需要删除元素的个数(包括start在内), 没有这个参数则删除从start下标开始之后所有的元素
// item1, item2是在做删除操作之后, 在start下标元素之后插入这些元素
var months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ['Jan', 'Feb', 'March', 'April', 'June']

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ['Jan', 'Feb', 'March', 'April', 'May']
```

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
