---
title: 如何使用 SQL 替换字段中的内容
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.DaintreeRiver_ZH-CN2284362798_1920x1080.jpg'
date: 2019-09-11 20:12:30
tags:
- replace
categories:
- sql
---

最近写了一个爬虫, 并需要去除一些内容, 用的是 `python` 的 `replace ` 方法,
但这个方法不会改变原有字符串, 所以数据库中存了很多 `不干净` 的数据, 但数据太多了,
删掉重新爬取需要很多时间, 那如何使用 `sql` 将这些数据更新一下呢?

<!-- more -->

```sql
update your_table set your_column = replace(your_column, 'old_str', 'new_str')

```

参考: https://www.w3cschool.cn/sql/sql-replace.html
