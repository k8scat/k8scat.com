---
title: 配置 npm 的镜像
comments: true
date: 2019-06-14 23:58:05
tags:
- registry
- cnpm
categories:
- npm
thumbnail: https://source.unsplash.com/uVZO1f5taCY
---

如何配置 `npm` 的镜像(淘宝)加快 `install` 的速度?

<!-- more -->

```shell
npm config set registry https://registry.npm.taobao.org
```

Refer: https://npm.taobao.org/

