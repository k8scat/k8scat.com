---
title: 现代开发者必须掌握的自动化构建部署流程
comments: true
date: 2019-06-18 15:13:10
tags:
- CI/CD
categories:
- DevOps
thumbnail: https://source.unsplash.com/tClS6SPq8Zo
toc: true
---

IDEA + GitHub/Gitee/GitLab/Gitea + Jenkins + Docker + Linux = CI/CD

<!-- more -->

## IDEA 非常强大的IDE

下载地址: https://www.jetbrains.com/idea/

## GitHub/Gitee/GitLab/Gitea 代码托管平台

GitHub: https://github.com/

Gitee: https://gitee.com/

GitLab: https://about.gitlab.com/

Gitea: https://gitea.io/zh-cn/

## Jenkins 流行的持续集成工具

地址: https://jenkins.io/zh/

## Docker 容器化

官方文档: https://docs.docker.com/

## Linux 项目部署的服务器

Centos: https://www.centos.org/

Ubuntu: https://ubuntu.com/

## CI/CD 持续集成/持续部署 (简化)

使用IDEA进行编程 -> 单元测试 -> 推送到指定仓库 -> 触发Webhook -> Jenkins进行CI/CD -> 项目上线

## 慕课网的免费课程

用Jenkins自动化搭建测试环境: https://www.imooc.com/learn/1008

Jenkins+K8s实现持续集成: https://www.imooc.com/learn/1112
