---
title: hexo 博客部署失败
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.WorldOctopus_ZH-CN2670477302_UHD.jpg'
toc: true
date: 2019-10-10 18:56:30
tags:
categories:
---

hexo 博客部署时出现错误: `TypeError: Cannot set property 'lastIndex' of undefined`

<!-- more -->

修改 `_config.yml` 中 `auto_detect` 的值为 `false`:

```yml
highlight: 
  enable: true
  line_number: false
  auto_detect: false
  tab_replace:

```

参考: https://github.com/hexojs/hexo/issues/2380
