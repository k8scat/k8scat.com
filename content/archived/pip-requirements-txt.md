---
title: 生成及使用 requirements.txt
comments: true
thumbnail: 'https://source.unsplash.com/OouFKZynpAM'
date: 2019-08-21 03:56:26
tags:
- pip
- requirements.txt
categories:
- python
---

平时使用的是 [pipenv](https://pipenv.readthedocs.io/en/latest/), 但是在 `docker` 构建镜像时, 其本身就是一个隔离的环境, 就没必要再去安装 `pipenv`, 这时如何安装需要的依赖呢?

<!-- more -->

```sh
# 生成 requirements.txt
pip freeze > requirements.txt

# 使用 requirements.txt 安装所需的依赖
pip install -r requirements.txt

```

[CSDNBot](https://github.com/hsowan/CSDNBot) web版的 Dockerfile:

```Dockerfile
FROM python:3.7

LABEL maintainer="hsowan<hsowan.me@gmail.com>"

WORKDIR /CSDNBot
COPY . .

RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/ -r requirements.txt && \
# 更新apt源
wget -O /etc/apt/sources.list https://gitee.com/hsowan/linux-run/raw/master/debian/buster/tsinghua/sources.list && \
apt update && \
apt install -y cron

EXPOSE 8000

```
