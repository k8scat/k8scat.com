---
title: NuxtJS 自定义 layout
comments: true
thumbnail: 'https://cdn.pixabay.com/photo/2018/04/04/08/11/mammal-3289124_1280.jpg'
date: 2019-11-02 11:24:10
tags:
- NuxtJS
- layout
categories:
- frontend
---

`NuxtJS` 的 `SPA` 项目怎么处理不同的布局?

<!-- more -->

```bash
# Use the layout key in your pages components to define which layout to use:
# In this example, Nuxt.js will include the layouts/blog.vue file as a layout for this page component.
export default {
  layout: 'blog',
  // OR
  layout (context) {
    return 'blog'
  }
}

```

Refer: 

* [API: The layout Property](https://nuxtjs.org/api/pages-layout)
