---
title: Ubuntu 无法更新
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.BubbleNebula_ZH-CN2787112807_1920x1080.jpg'
date: 2019-10-09 21:53:06
tags:
- apt
- update
categories:
- Ubuntu
---

在更换软件源时遇到了如下问题:

```yml
sudo apt-get update

E: Could not get lock /var/lib/apt/lists/lock - open (11: Resource temporarily unavailable)
E: Unable to lock directory /var/lib/apt/lists/

```

<!-- more -->

这表明当前有某个进程正在 `apt-get`, 然而我并没有使用任何命令, 于是需要kill掉进程。

解决方法是：

```bash
sudo rm /var/lib/apt/lists/lock

```

转载: https://www.cnblogs.com/qq952693358/p/6537846.html
