---
title: 'Docker: net/http: TLS handshake timeout' 
comments: true
thumbnail: 'https://source.unsplash.com/xNDFrCge8c4'
toc: true
date: 2019-08-01 17:32:17
tags:
- timeout
categories:
- Docker
---

使用 [Drone](https://drone.io/) 做 `CICD` 时提示 `net/http: TLS handshake timeout`, 如何解决?

<!-- more -->

配置镜像加速器即可

```sh
# ubuntu/centos
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["http://f1361db2.m.daocloud.io"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker

```

Refer:

* [DaoCloud 镜像站](https://www.daocloud.io/mirror)
* [阿里云容器镜像服务](https://cr.console.aliyun.com/cn-shenzhen/instances/mirrors)