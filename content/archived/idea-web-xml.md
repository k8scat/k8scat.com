---
title: 'IDEA 提示: servlet should have a mapping'
comments: true
thumbnail: 'https://source.unsplash.com/X_yDmjSAH4g'
toc: true
date: 2019-08-06 16:01:33
tags:
- IDEA
- web.xml
categories:
- IDEA
---

已经配置了 `servlet` 的 `mapping`, 但 `IDEA` 提示 `servlet should have a mapping`, 如何解决?

<!-- more -->

打开 `IDEA` 的 `Project Structure` 并添加 `web.xml` 的位置:

{% img /images/idea-web-xml.png %}




