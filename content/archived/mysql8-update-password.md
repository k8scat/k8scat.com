---
title: MySQL8 重置密码
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.EidolonHelvum_ZH-CN0881732109_1920x1080.jpg'
date: 2019-10-29 14:47:39
tags:
- password
- mysql8
categories:
- DevOps
---

MySQL8 忘记密码怎么办?

<!-- more -->

```bash

sudo vi /etc/mysql/my.cnf
# 添加以下内容
[mysqld]
skip-grant-tables

# 保存后重启MySQL
sudo service mysqld restart

# 无密码进入MySQL
mysql -uroot -p
mysql> alter user 'root'@'%' identified by '';
mysql> flush privileges;
mysql> exit

# 将配置文件复原
sudo vi /etc/mysql/my.cnf

# 注释掉之前添加的内容, 修改为以下内容
[mysqld]
# skip-grant-tables

# 重启MySQL
sudo service mysqld restart

# 空密码进入MySQL
mysql -uroot -p
mysql> alter user 'root'@'%' identified by '123456'
mysql> flush privileges;

```

参考:
* [CentOS 安装 MySQL](https://hsowan.me/2019/10/28/centos-install-mysql/)
* [mysql什么时候需要flush privileges](https://zhidao.baidu.com/question/326400372830726925.html)
