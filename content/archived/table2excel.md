---
title: 如何将网页中的 table 导出成 excel
comments: true
thumbnail: 'https://source.unsplash.com/v1XoQijG5xU'
date: 2019-08-30 16:23:37
tags:
- table
- excel
categories:
- github
---

如何将[目标网页](http://job.hust.edu.cn/bjssygk23213.htm)中的 `table` 导出成 `excel`?

<!-- more -->

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>table2excel</title>
</head>
<body>

<input type="button" id="btn" value="导出"/>

<table id="example">
  省略内容
</table>

<script src="jquery.min.js"></script>
<script src="jquery.table2excel.min.js"></script>
<script>
  $(function() {
    $('#btn').click(function() {
      $('#example').table2excel({
        // 不被导出的表格行的CSS class类
        exclude: '.excludeThisClass',
        // 导出的Excel文档的名称，（没看到作用）
        name: 'Worksheet Name',
        // Excel文件的名称
        filename: 'SomeFile.xls'
      })
    })
  })
</script>
</body>
</html>
```

案例地址: https://github.com/hsowan/table2excel 