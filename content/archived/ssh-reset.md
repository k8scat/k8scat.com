---
title: Jenkins SSH Reset
comments: true
date: 2019-07-26 10:04:58
tags:
- jenkins
- reset
categories:
- SSH
thumbnail: https://cn.bing.com/th?id=OHR.SurfboardRow_ZH-CN5154549470_UHD.jpg
---

Jenkins连接节点的时候一直报`ssh reset`, 如何解决?

<!-- more -->

```bash
# 去掉相应的ip即可
vi sshd.deny.hosteye
```
