---
title: 允许远程调用 Docker API
comments: true
thumbnail: 'https://source.unsplash.com/lR-oW3b23cM'
date: 2019-07-30 17:16:03
tags:
- Docker
- Portainer
- Endpoint URL
categories:
- Docker
---

使用 [Portainer](https://github.com/portainer/portainer/) 管理容器时, 需要设置 `Endpoint URL`, 也就是远程调用 `Docker API` 的 `host`, 但默认 `Docker 实例`是不允许的, 如何解决?

<!-- more -->

```
# 创建目录
mkdir /etc/systemd/system/docker.service.d

# 创建文件 *.conf
vi /etc/systemd/system/docker.service.d/tcp.conf

# 添加以下内容
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376

# 重新加载配置文件
sudo systemctl daemon-reload

# 重启 docker 实例
systemctl restart docker.service

# 以下两种方式可以检查是否配置成功
ps aux | grep docker
> root      3695  0.0  1.8 724008 74616 ?        Ssl  16:12   0:04 /usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376

netstat -lnp | grep 2376
> tcp6       0      0 :::2376                 :::*                    LISTEN      3695/dockerd

```

则 Endpoint URL: `$ip:2376`

我没有使用 `TLS`, 如果需要可以参考: https://docs.docker.com/engine/security/https/

Mac 下开启docker API远程调用:

```shell
docker run -d -v /var/run/docker.sock:/var/run/docker.sock -p 2376:2375 bobrik/socat TCP4-LISTEN:2375,range=0.0.0.0/0,fork,reuseaddr UNIX-CONNECT:/var/run/docker.sock
```

Refer:
* [How do I enable the remote API for dockerd](https://success.docker.com/article/how-do-i-enable-the-remote-api-for-dockerd)
* [Docker打开TCP管理端口](https://blog.csdn.net/onlyshenmin/article/details/81069047)
* [Configure and troubleshoot the Docker daemon](https://docs.docker.com/config/daemon/)
* [Remote API with Docker for Mac (BETA)](https://forums.docker.com/t/remote-api-with-docker-for-mac-beta/15639)
