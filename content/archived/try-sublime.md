---
title: Sublime 的使用
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.SunbeamsForest_ZH-CN5358008117_1920x1080.jpg'
date: 2019-09-18 17:08:50
tags:
- sublime
categories:
- tools
---

厌倦 `vsc`  了, 换 `sublime` 用用!

<!-- more -->

添加命令行启动(mac)

```
# 我用的是zsh
vi ~/.zshrc

# 添加以下内容
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"

# 生效
source ~/.zshrc

```


