---
title: Docker 批量删除镜像或容器
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.ArroyoGrande_ZH-CN2178202888_1920x1080.jpg'
toc: true
date: 2019-09-07 18:05:19
tags:
- rm
categories:
- docker
---

<!-- more -->

```sh
# 删除所有<none>镜像
docker rmi `docker images | grep '<none>' | awk '{print $3}'`

# 删除所有已退出的容器
docker rm `docker ps -a | grep Exited | awk '{print $1}'`

```