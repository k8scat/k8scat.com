---
title: CentOS 安装 MySQL
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.FortRockHomestead_ZH-CN0775183699_1920x1080.jpg'
date: 2019-10-28 10:56:22
tags:
- MySQL
- CentOS
categories:
- DevOps
---

`MySQL` 被 `Oracle` 收购后，`CentOS` 的镜像仓库中提供的默认的数据库也变为了 `MariaDB`,
那如何在 `CentOS` 上如何安装 `MySQL` 呢?

<!-- more -->

现在一般都是用 `Docker`, 跑个 `MySQL`:
`docker run -d --name mysql8 -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 mysql:8`.

> 本文安装的是 `MySQL8`

```bash
# CentOS7 阿里源
sudo mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
sudo wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
sudo yum update -y

# centos7
# 下载mysql的rpm包: https://dev.mysql.com/downloads/repo/yum/
wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
# 安装rpm包
sudo rpm -ivh mysql80-community-release-el7-3.noarch.rpm
# 安装MySQL
sudo yum install mysql-community-server

# 启动MySQL
sudo service mysqld start
# 查看MySQL状态
# service mysqld status

# 获取临时密码
sudo cat /var/log/mysqld.log | grep 'temporary password'
> 2019-10-28T04:31:58.792853Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: SAUraYtA3h+s

# MySQL 配置文件
# mysql --help
Default options are read from the following files in the given order:
/etc/my.cnf /etc/mysql/my.cnf /usr/etc/my.cnf ~/.my.cnf


# 进入MySQL
mysql -uroot -p

# 查看当前安全变量值, PS: 这是我修改后的查看的, MySQL8要在修改临时密码后才可以执行下面的命令
# 另外, MySQL57 用的是下划线, 例如: validate_password_policy
mysql> show variables like 'validate_password%';
+--------------------------------------+--------+
| Variable_name                        | Value  |
+--------------------------------------+--------+
| validate_password.check_user_name    | ON     |
| validate_password.dictionary_file    |        |
| validate_password.length             | 8      |
| validate_password.mixed_case_count   | 1      |
| validate_password.number_count       | 1      |
| validate_password.policy             | MEDIUM |
| validate_password.special_char_count | 1      |
+--------------------------------------+--------+
7 rows in set (0.01 sec)

# 设置密码策略, validate_password.policy
# 0 or LOW: Length
# 1 or MEDIUM: Length; numeric, lowercase/uppercase, and special characters
# 2 or STRONG: Length; numeric, lowercase/uppercase, and special characters; dictionary file
# global: 全局配置, 但是在MySQL重启后配合
mysql> set global validate_password.policy = 0;
# 设置密码至少4位, validate_password.length
mysql> set global validate_password.length = 4;

# 成功修改密码为 123456
mysql> alter user 'root'@'localhost' identified by '123456'

mysql> select user, host, authentication_string from user;
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
4 rows in set (0.00 sec)

# 设置所有的IP都可以连接MySQL
mysql> update user set host = '%' where user = 'root';

mysql> select user, host, plugin from user;
+------------------+-----------+-----------------------+
| user             | host      | plugin                |
+------------------+-----------+-----------------------+
| root             | %         | mysql_native_password |
| mysql.infoschema | localhost | caching_sha2_password |
| mysql.session    | localhost | caching_sha2_password |
| mysql.sys        | localhost | caching_sha2_password |
+------------------+-----------+-----------------------+
4 rows in set (0.00 sec)

# 图形化工具(Navicat)无法连接数据库
# MySQL8 的密码认证插件: caching_sha2_password
# MySQL57: mysql_native_password
mysql> alter user 'root'@'%' identified with mysql_native_password by '123456';

# 上面的命令会出现问题: ERROR 1524 (HY000): Plugin 'mysql_native_plugin' is not loaded
# 通过下面的命令设置认证插件
mysql> update user set plugin='mysql_native_password' where user = 'root'; 
mysql> alter user 'root'@'%' identified by '123456';
mysql> flush privileges;
mysql> exit

```

参考:
* [CentOS安装配置MySQL8.0的步骤详解](https://www.jb51.net/article/145036.htm)
* [ERROR 1819 (HY000): Your password does not satisfy the current policy requirements](https://www.cnblogs.com/ivictor/p/5142809.html)
* [MySQL8新特性-PERSIST](http://www.royalwzy.com/?p=1363)
* [mysql8.0 设置简单密码报错ERROR 1819 (HY000): Your password does not satisfy the current policy requirements](https://www.jianshu.com/p/b437566ccf98)