---
title: Git 小技巧 - 如何优雅地合并分支
comments: true
date: 2019-07-21 20:39:42
tags:
- 合并分支
categories:
- Git
thumbnail: https://source.unsplash.com/gUIJ0YszPig
---

如何合并分支并只保留一个 `commit` ?

<!-- more -->

```bash
# 从主分支切换到新的分支
$ git checkout -b new-branch

# 上面的命令等同于下面两条命令
$ git branch new-branch
$ git checkout new-branch

# 在新分支上提交了n个commit
# 准备合并到主分支
# 先切回主分支
$ git checkout master

# 然后进行合并
# 这种情况下的合并操作没有需要解决的分歧 - 这种合并就叫 'Fast-forward'
$ git merge new-branch

# 但如果再从主分支切出一个新分支
$ git checkout -b new-branch-1

# 然后在第一个分支已经合并到主分支的情况下
# 把第二个新的分支合并到主分支
# 这里就可能会产生冲突
# 需要手动解决冲突, 然后再做提交即可
$ git merge new-branch-1

# 上面的都不是重点
# 这里要讲的是合并的时候如果新分支提交了多个commit, 如何只保留一个commit到主分支上
# --squash create a single commit instead of doing a merge
$ git merge --squash new-branch
$ git commit -m "your commit"

```

Refer: [3.2 Git 分支 - 分支的新建与合并](https://git-scm.com/book/zh/v2/Git-%E5%88%86%E6%94%AF-%E5%88%86%E6%94%AF%E7%9A%84%E6%96%B0%E5%BB%BA%E4%B8%8E%E5%90%88%E5%B9%B6)