---
title: crontab 无法执行 docker-compose 命令
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.AutumnTreesNewEngland_ZH-CN1766405773_UHD.jpg'
toc: true
date: 2019-09-11 20:40:04
tags:
- crontab
categories:
- DevOps
---

`0 0 * * * cd /somewhere && docker-compose up -d some-service` 像这样的定时任务没法正常启动?

<!-- more -->
`This is unlikely to be an issue with docker-compose. It's good practice to use absolute paths in crontabs because your $PATH may not be the same as an interactive shell. Maybe add set -x to see why it's failing?`

```
# whereis docker-compose
0 0 * * * cd /somewhere && /usr/local/bin/docker-compose up -d some-service

```

Refer: https://github.com/docker/compose/issues/2293