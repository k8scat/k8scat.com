---
title: 查看 Ubuntu 的版本信息
comments: true
date: 2019-06-14 20:30:51
tags:
- ubuntu
- 版本信息
- lsb_release
categories:
- DevOps
thumbnail: https://source.unsplash.com/w-bPRl6xNPs
toc: true
---

如何

<!-- more -->

```shell
$ lsb_release -a

No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 16.04.6 LTS
Release:	16.04
Codename:	xenial
```
