---
title: Oracle Java SE Support Roadmap
comments: true
date: 2019-04-19 15:44:26
tags:
- oracle
- JavaSE
- JDK
categories:
thumbnail: https://source.unsplash.com/T8LMIN09-mo
toc: true
---

<!-- more -->

{% img /images/oracle-java-se-support-roadmap.png %}

https://www.oracle.com/technetwork/java/java-se-support-roadmap.html
