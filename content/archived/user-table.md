---
title: 严重批判 phone 和 password 自成一家
comments: true
thumbnail: 'https://cn.bing.com/th?id=OHR.ElMorro_ZH-CN1911346184_UHD.jpg'
date: 2019-09-02 19:57:08
tags:
- user
- phone
- password
categories:
- design
---

手上有这么一个项目, `user` 表里只有 `phone` 和 `password` 这个两个字段(除了 `id`, `create_time`, `update_time` 这些字段), 其他信息(比如 `name`, `gender` 等字段)保存在 `user_profile` 表里, 为什么要这样做呢? 放在一个表里不就ok了吗?

<!-- more -->

经过强烈讨论看文章和思考后, 就写了这篇文章, 欢迎指正和交流

这涉及到 `分表分库` 解决 `支持高并发, 数据量大` 的问题了, 什么是分表分库? 参考这篇文章: https://juejin.im/entry/5b5eb7f2e51d4519700f7d3c

其中垂直拆分的意思，就是把一个有很多字段的表给拆分成多个表，或者是多个库上去。每个库表的结构都不一样，每个库表都包含部分字段。一般来说，会将 `较少的访问频率很高的字段` 放到一个表里去，然后将 `较多的访问频率很低的字段` 放到另外一个表里去。因为数据库是有缓存的，你访问频率高的行字段越少，就可以在缓存里缓存更多的行，性能就越好。这个一般在表层面做的较多一些。

就目前这个情况, `user` 表目的在于登录业务上(该项目只支持手机号登录), `user_profile` 表目的在于用户数据分析, 内容推送等, 该表的访问频率对于 `user` 表是更高的, 所以 `phone 和 password 自成一家` 想想也是可以的耶。

