---
title: "LeetCode Go - 1. 两数之和"
date: 2021-06-17T23:53:35+08:00
weight: 1
aliases: ["/first"]
tags: ["go", "数组", "哈希表", "简单"]
categories: ["leetcode-go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/leetcode-go.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

为了更好的明天，坚持刷 [LeetCode](https://leetcode-cn.com/u/k8scat/)！

<!--more-->

## 题目

给定一个整数数组 `nums` 和一个整数目标值 `target`，请你在该数组中找出 和为目标值 target 的那 **两个** 整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

你可以按任意顺序返回答案。

示例 1：

```text
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
```

示例 2：

```text
输入：nums = [3,2,4], target = 6
输出：[1,2]
```

示例 3：

```text
输入：nums = [3,3], target = 6
输出：[0,1]
```

提示：

```text
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
只会存在一个有效答案
```

## 解法一 - 暴力枚举

### 解题思路

使用嵌套循环，第一层循环从下标 0 开始，第二层循环从第一层循环的下标 +1 开始，每次循环判断两数之和是否等于 `target`。

### 代码

```go
func twoSum(nums []int, target int) []int {
    for i, n1 := range nums {
        for j := i+1; j < len(nums); j++ {
            if n1 + nums[j] == target {
                return []int{i, j}
            }
        }
    }
    return nil
}
```

### 执行结果

```text
执行用时：40 ms，在所有 Go 提交中击败了 5.72% 的用户
内存消耗：3.6 MB，在所有 Go 提交中击败了 38.74% 的用户
```

### 复杂度分析

- 时间复杂度：$O(n^2)$，其中 $n$ 是数组中的元素数量。最坏情况下数组中任意两个数都要被匹配一次。
- 空间复杂度：$O(1)$。

## 解法二 - 哈希表

哈希表的解法也是这道题的最优解。

### 解题思路

解法一中我们在第一层循环中拿到 `x`，在第二层循环中拿到 `y`，并判断 `x + y` 是否等于 `target`，也就是想找到 `target - x`，所以我们可以利用一个 **哈希表** 来保存元素，每次循环拿到 `x`，判断哈希表里面是否存在 `target - x`，如果不存在，就把 `x` 和 `x` 对应的下标存入哈希表，如果存在，则表示找到 `x + y = target` 的两个数，此时返回两个数的下标。

### 代码

```go
func twoSum(nums []int, target int) []int {
    m := make(map[int]int)
    for i, n := range nums {
        if j, found := m[target-n]; found {
            return []int{j, i}
        }
        m[n] = i
    }
    return nil
}
```

### 执行结果

```text
执行用时：8 ms，在所有 Go 提交中击败了 37.21% 的用户
内存消耗：4.2 MB，在所有 Go 提交中击败了 10.15% 的用户
```

### 复杂度分析

- 时间复杂度：$O(n)$，其中 $n$ 是数组中的元素数量。对于每一个元素 `x`，我们可以 O(1)（利用**哈希表**）地寻找 `target - x`。
- 空间复杂度：$O(n)$，其中 $n$ 是数组中的元素数量。主要为哈希表的开销。

## 题目链接

[1. 两数之和](https://leetcode-cn.com/problems/two-sum/)

## 相似题目

- [三数之和](https://leetcode-cn.com/problems/3sum/)
- [四数之和](https://leetcode-cn.com/problems/4sum/)
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
- [两数之和 III - 数据结构设计](https://leetcode-cn.com/problems/two-sum-iii-data-structure-design/)
- [和为K的子数组](https://leetcode-cn.com/problems/subarray-sum-equals-k/)
- [两数之和 IV - 输入 BST](https://leetcode-cn.com/problems/two-sum-iv-input-is-a-bst/)
- [小于 K 的两数之和](https://leetcode-cn.com/problems/two-sum-less-than-k/)
