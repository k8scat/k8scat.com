---
title: "`LeetCode Go - 7. 整数反转`"
date: 2021-06-17T23:53:35+08:00
weight: 1
aliases: ["/first"]
tags: ["go", "数学", "简单"]
categories: ["leetcode-go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/leetcode-go.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

为了更好的明天，坚持刷 [LeetCode](https://leetcode-cn.com/u/k8scat/)！

<!--more-->

## 题目

给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。

假设环境不允许存储 64 位整数（有符号或无符号）。

示例 1：

```text
输入：x = 123
输出：321
```

示例 2：

```text
输入：x = -123
输出：-321
```

示例 3：

```text
输入：x = 120
输出：21
```

示例 4：
 
```text
输入：x = 0
输出：0
```

提示：

```text
-231 <= x <= 231 - 1
```

## 解法一 - 数学

### 解题思路

每次获取 $x$ 的最后一位，并追加到另一位数 $rev$ 的最后一位上：

```text
# 获取最后一位
last = x % 10
x = x / 10

# 追加到 rev 的最后一位上
rev = rev * 10 + last
```

### 代码

```go
func reverse(x int) (rev int) {
    for x != 0 {
        if rev < math.MinInt32/10 || rev > math.MaxInt32/10 {
            return 0
        }
        digit := x % 10
        x /= 10
        rev = rev*10 + digit
    }
    return
}
```

### 执行结果

```text
执行用时：0 ms，在所有 Go 提交中击败了 100.00% 的用户
内存消耗：2.1 MB，在所有 Go 提交中击败了 63.53% 的用户
```

### 复杂度分析

- 时间复杂度：$O(log∣x∣)$。翻转的次数即 $x$ 十进制的位数。
- 空间复杂度：$O(1)$

## 题目链接

[7. 整数反转](https://leetcode-cn.com/problems/reverse-integer/)

## 相似题目

- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
- [颠倒二进制位](https://leetcode-cn.com/problems/reverse-bits/)
