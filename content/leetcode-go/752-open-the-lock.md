---
title: "LeetCode Go - 752. 打开转盘锁"
date: 2021-06-17T23:53:35+08:00
weight: 1
aliases: ["/first"]
tags: ["go", "广度优先搜索", "数组", "哈希表", "字符串", "中等"]
categories: ["leetcode-go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/leetcode-go.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

为了更好的明天，坚持刷 [LeetCode](https://leetcode-cn.com/u/k8scat/)！

<!--more-->

## 题目

你有一个带有四个圆形拨轮的转盘锁。每个拨轮都有10个数字： '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' 。每个拨轮可以自由旋转：例如把 '9' 变为 '0'，'0' 变为 '9' 。每次旋转都只能旋转一个拨轮的一位数字。

锁的初始数字为 '0000' ，一个代表四个拨轮的数字的字符串。

列表 deadends 包含了一组死亡数字，一旦拨轮的数字和列表里的任何一个元素相同，这个锁将会被永久锁定，无法再被旋转。

字符串 target 代表可以解锁的数字，你需要给出解锁需要的最小旋转次数，如果无论如何不能解锁，返回 -1 。

示例 1:

```text
输入：deadends = ["0201","0101","0102","1212","2002"], target = "0202"
输出：6
解释：
可能的移动序列为 "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202"。
注意 "0000" -> "0001" -> "0002" -> "0102" -> "0202" 这样的序列是不能解锁的，
因为当拨动到 "0102" 时这个锁就会被锁定。
```

示例 2:

```text
输入: deadends = ["8888"], target = "0009"
输出：1
解释：
把最后一位反向旋转一次即可 "0000" -> "0009"。
```

示例 3:

```text
输入: deadends = ["8887","8889","8878","8898","8788","8988","7888","9888"], target = "8888"
输出：-1
解释：
无法旋转到目标数字且不被锁定。
```

示例 4:

```text
输入: deadends = ["0000"], target = "8888"
输出：-1
``` 

提示：

```text
1 <= deadends.length <= 500
deadends[i].length == 4
target.length == 4
target 不在 deadends 之中
target 和 deadends[i] 仅由若干位数字组成
```

## 解法一 - BFS

### 解题思路

使用队列进行广度优先搜索，设当前搜索到的数字为 `status`，旋转的次数为 `step`，我们可以枚举 `status` 通过一次旋转得到的数字。设其中的某个数字为 `next_status`，如果其没有被搜索过，我们就将 (next_status,step+1) 加入队列。如果搜索到了 target，我们就返回其对应的旋转次数 `step`，同时使用哈希表存储已经搜索过中的状态。如果没有搜索到 `target`，返回 -1。

### 代码

```go
func openLock(deadends []string, target string) int {
    const start = "0000"
    if target == start {
        return 0
    }

    dead := map[string]bool{}
    for _, s := range deadends {
        dead[s] = true
    }
    if dead[start] {
        return -1
    }

    // 枚举 status 通过一次旋转得到的数字
    get := func(status string) (ret []string) {
        s := []byte(status)
        for i, b := range s {
            s[i] = b - 1
            if s[i] < '0' {
                s[i] = '9'
            }
            ret = append(ret, string(s))
            s[i] = b + 1
            if s[i] > '9' {
                s[i] = '0'
            }
            ret = append(ret, string(s))
            s[i] = b
        }
        return
    }

    type pair struct {
        status string
        step   int
    }
    q := []pair{{start, 0}}
    seen := map[string]bool{start: true}
    for len(q) > 0 {
        p := q[0]
        q = q[1:]
        for _, nxt := range get(p.status) {
            if !seen[nxt] && !dead[nxt] {
                if nxt == target {
                    return p.step + 1
                }
                seen[nxt] = true
                q = append(q, pair{nxt, p.step + 1})
            }
        }
    }
    return -1
}
```

### 执行结果

```text

```

### 复杂度分析

- 时间复杂度：
- 空间复杂度：

## 题目链接

[752. 打开转盘锁](https://leetcode-cn.com/problems/open-the-lock/)

## 相似题目
