---
title: "LeetCode Go - 2. 两数相加"
date: 2021-06-18T23:56:03+08:00
weight: 1
aliases: ["/first"]
tags: ["go", "递归", "链表", "数学", "中等"]
categories: ["leetcode-go"]
author: "K8sCat"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://raw.githubusercontent.com/storimg/img/master/k8scat.com/leetcode-go.png"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

为了更好的明天，坚持刷 [LeetCode](https://leetcode-cn.com/u/k8scat/)！

<!--more-->

## 题目

给你两个 **非空** 的链表，表示两个非负的整数。它们每位数字都是按照 **逆序** 的方式存储的，并且每个节点只能存储 **一位** 数字。

请你将两个数相加，并以相同形式返回一个表示和的链表。

你可以假设除了数字 0 之外，这两个数都不会以 0 开头。

示例 1：

![leetcode-add-two-numbers](https://raw.githubusercontent.com/storimg/img/master/leetcode-cn.com/leetcode-add-two-numbers.jpeg)

```text
输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[7,0,8]
解释：342 + 465 = 807.
```

示例 2：

```text
输入：l1 = [0], l2 = [0]
输出：[0]
```

示例 3：

```text
输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
输出：[8,9,9,9,0,0,0,1]
```

## 解法一 - 暴力循环

### 解题思路

首先明确两个数在链表中的存放顺序的 **逆序** 的，所以头节点指向的是每个数的最后一位（即个位数），那我们就可以将两个链表的头节点的 `Val` 进行相加，同时分别将两个链接的头节点指向它们的子节点 `Next`。

当然，在相加时我们还需要考虑到 **进位**，进位等于 `(n1 + n2 + carry) / 10`。

### 代码

我们用 `head` 表示输出链表的头节点，`tail` 始终指向输出链表的尾结点：

```go
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1, l2 *ListNode) (head *ListNode) {
    var tail *ListNode
    carry := 0
    for l1 != nil || l2 != nil {
        n1, n2 := 0, 0
        if l1 != nil {
            n1 = l1.Val
            l1 = l1.Next
        }
        if l2 != nil {
            n2 = l2.Val
            l2 = l2.Next
        }
        sum := n1 + n2 + carry
        sum, carry = sum%10, sum/10
        if head == nil {
            head = &ListNode{Val: sum}
            tail = head
        } else {
            tail.Next = &ListNode{Val: sum}
            tail = tail.Next
        }
    }
    if carry > 0 {
        tail.Next = &ListNode{Val: carry}
    }
    return
}
```

### 执行结果

```text
执行用时：24 ms，在所有 Go 提交中击败了 10.96% 的用户
内存消耗：4.7 MB，在所有 Go 提交中击败了 22.11% 的用户
```

### 复杂度分析

- 时间复杂度：$O(max(m,n))$，其中 $m$ 和 $n$ 分别为两个链表的长度。我们要遍历两个链表的全部位置，而处理每个位置只需要 $O(1)$ 的时间。
- 空间复杂度：$O(1)$。注意返回值不计入空间复杂度。

## 解法二 - 递归

### 解题思路

从解法一我们可以发现，每次循环都是将两个相同位上的数相加，同时算出进位是多少，那我们就可以写一个递归函数，并将每次的进位作为参数进行传递：

- 递归函数返回相同位上的两数相加后的节点
- 递归函数在 `carry` 为零，且当其中有一个或两个链表为空的时候终止

### 代码

```go
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1, l2 *ListNode) (head *ListNode) {
    return add(l1, l2, 0)
}

func add(l1, l2 *ListNode, carry int) *ListNode {
    if l1 == nil && l2 == nil && carry == 0 {
        return nil
    }
    if l1 != nil && l2 == nil && carry == 0 {
        return l1
    }
    if l2 != nil && l1 == nil && carry == 0 {
        return l2
    }

    sum := carry
    if l1 != nil {
        sum += l1.Val
        l1 = l1.Next
    }
    if l2 != nil {
        sum += l2.Val
        l2 = l2.Next
    }
    sum, carry = sum%10, sum/10
    node := &ListNode{Val: sum}
    node.Next = add(l1, l2, carry)
    return node
}
```

### 执行结果

```text
执行用时：16 ms，在所有 Go 提交中击败了 38.47% 的用户
内存消耗：4.7 MB，在所有 Go 提交中击败了 12.74% 的用户
```

### 复杂度分析

- 时间复杂度：$O(min(m,n))$，其中 $m$ 和 $n$ 分别为两个链表的长度。当其中一个或两个链表为空时，递归终止。
- 空间复杂度：$O(min(m,n))$。

## 题目链接

[2. 两数相加](https://leetcode-cn.com/problems/add-two-numbers/)

## 相似题目

- [字符串相乘](https://leetcode-cn.com/problems/multiply-strings/)
- [二进制求和](https://leetcode-cn.com/problems/add-binary/)
- [两整数之和](https://leetcode-cn.com/problems/sum-of-two-integers/)
- [字符串相加](https://leetcode-cn.com/problems/add-strings/)
- [两数相加 II](https://leetcode-cn.com/problems/add-two-numbers-ii/)
- [数组形式的整数加法](https://leetcode-cn.com/problems/add-to-array-form-of-integer/)
