#!/bin/bash
# 基于 BASE_BRANCH 创建一个分支，并切换到该分支，然后创建一个新的文章，推送到远程
# 同时向 master 分支提交一个 PR，当写完文章后，进行 PR 审查，最终合并到 master 分支
# master 分支会进行自动构建部署 GitHub Pages
#
# Deprecated 太复杂了
set -ex

POST_PATH=""
POST_NAME=""
POST_TYPE=""
BRANCH=""

BASE_BRANCH="master"

function prepare() {
    POST_PATH=$1
    if [[ -z "${POST_PATH}" ]]; then
        echo "usage: $0 <POST_PATH>"
        exit 1
    fi
    POST_NAME=$(echo ${POST_PATH} | awk -F/ '{print $2}')
    if [[ -z "${POST_NAME}" ]]; then
        echo "POST_NAME not found: ${POST_PATH}"
        exit 1
    fi
    POST_TYPE=$(echo ${POST_PATH} | awk -F/ '{print $1}')
    if [[ -z "${POST_TYPE}" ]]; then
        echo "POST_TYPE not found: ${POST_PATH}"
        exit 1
    fi
}

function checkout() {
    BRANCH="post_${POST_TYPE}_${POST_NAME}"
    git checkout ${BASE_BRANCH}
    git checkout -b ${BRANCH}
}

function create_post() {
    local post="posts/${POST_PATH}.md"
    local post_file="content/${post}"
    local commit_message="add(${POST_TYPE}): ${post_file}"
    
    hugo new ${post}

    os="$(uname -s)"
    if [[ "${os}" = "Darwin" ]]; then
        sed -i "" "s/{category}/${POST_TYPE}/g" ${post_file}
    else
        sed -i "s/{category}/${POST_TYPE}/g" ${post_file}
    fi

    git add ${post_file}
    git commit -m "${commit_message}"
    git push origin ${BRANCH}
}

function create_pr() {
    local title="${POST_TYPE}: ${POST_NAME}"
    local base_branch="master"
    local assignee="@me"
    gh pr create \
        -t "${title}" \
        --base "${base_branch}" \
        --body "${title}" \
        -a "${assignee}"
}

function main() {
    prepare "$@"
    checkout
    create_post
    create_pr
}

main "$@"
