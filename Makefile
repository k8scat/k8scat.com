HUGO = hugo
GIT = git
PORT = 3388

.PHONY: serve
serve:
	$(HUGO) server -D --port $(PORT) --bind 0.0.0.0

.PHONY: build
build: update-submodule
	$(HUGO) --buildDrafts --gc --verbose --minify

.PHONY: update-submodule
update-submodule: download-submodule
	$(GIT) submodule update --remote --merge

.PHONY: download-submodule
download-submodule:
	$(GIT) submodule update --init --recursive

POST =
.PHONY: new-post
new-post:
ifneq ($(POST),)
	$(HUGO) new posts/$(POST).md
else
	@echo "Usage: make new-post POST=<post-name>"
endif

POST =
.PHONY: publish
publish:
ifneq ($(POST),)
	acli juejin article create $(POST)
	acli oschina article create $(POST)
	acli csdn article create $(POST)
else
	@echo "Usage: make publish POST=<post-file>"
endif
